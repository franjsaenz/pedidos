package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Presale;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.Status;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MCustomer;
import com.fsquiroz.pedidos.entity.json.MStats;
import com.fsquiroz.pedidos.entity.json.MUser;
import com.fsquiroz.pedidos.repository.PresaleStatusRepository;
import com.fsquiroz.pedidos.repository.SearchRepository;
import com.fsquiroz.pedidos.service.declaration.ICustomerService;
import com.fsquiroz.pedidos.service.declaration.IUserService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class StatService {

    private static final SimpleDateFormat YEAR_MONTH = new SimpleDateFormat("yyyy/MM");

    private IUserService userService;

    private ICustomerService customerService;

    private SearchRepository searchRepository;

    private PresaleStatusRepository presaleStatusRepository;

    public StatService(IUserService userService, ICustomerService customerService, SearchRepository searchRepository, PresaleStatusRepository presaleStatusRepository) {
        this.userService = userService;
        this.customerService = customerService;
        this.searchRepository = searchRepository;
        this.presaleStatusRepository = presaleStatusRepository;
    }

    public MStats getStats(Shop shop) {
        MStats ms = new MStats();
        ms.setByStatus(getCounters(shop));
        setCustomersAndUsers(shop, ms);
        ms.setMonthly(getMonthly(shop));
        return ms;
    }

    private MStats.MStatsStatus getCounters(Shop s) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        trimTime(c);
        Date from = c.getTime();
        c.add(Calendar.MONTH, 1);
        c.add(Calendar.DAY_OF_YEAR, -1);
        Date to = c.getTime();
        c.add(Calendar.YEAR, -1);
        c.add(Calendar.MONTH, 1);
        Date fromYear = c.getTime();
        MStats.MStatsStatus mss = new MStats.MStatsStatus();
        mss.setPending(getStatusSingle(s, Status.PENDING, from, fromYear, to));
        mss.setSent(getStatusSingle(s, Status.SENT, from, fromYear, to));
        mss.setDeliver(getStatusSingle(s, Status.DELIVER, from, fromYear, to));
        mss.setCancel(getStatusSingle(s, Status.CANCEL, from, fromYear, to));
        return mss;
    }

    private void setCustomersAndUsers(Shop shop, MStats ms) {
        Map<Customer, Long> customers = new TreeMap<>();
        Map<User, Long> users = new TreeMap<>();
        Pageable page = PageRequest.of(0, 50);
        Page<Presale> presales = searchRepository.searchPresales(shop, null, null, page);
        boolean hasNext = true;
        while (hasNext) {
            for (Presale p : presales) {
                append(customers, users, p);
            }
            hasNext = presales.hasNext();
            if (hasNext) {
                page = presales.nextPageable();
                presales = searchRepository.searchPresales(shop, null, null, page);
            }
        }
        List<MStats.MStatusCustomer> mscs = new ArrayList();
        List<MStats.MStatusUser> msus = new ArrayList<>();
        for (Map.Entry<Customer, Long> entry : customers.entrySet()) {
            MCustomer mc = customerService.build(entry.getKey());
            Long t = entry.getValue();
            mscs.add(new MStats.MStatusCustomer(mc, t));
        }
        for (Map.Entry<User, Long> entry : users.entrySet()) {
            MUser mu = userService.build(entry.getKey());
            Long t = entry.getValue();
            msus.add(new MStats.MStatusUser(mu, t));
        }
        Collections.sort(mscs);
        Collections.sort(msus);
        if (mscs.size() > 10) {
            mscs = mscs.subList(0, 10);
        }
        if (msus.size() > 10) {
            msus = msus.subList(0, 10);
        }
        ms.setByCustomer(mscs);
        ms.setByUser(msus);
    }

    private MStats.MStatusMonthly getMonthly(Shop shop) {
        List<MStats.MStatusDatePair> pairs = getDatePair();
        List<String> labels = new ArrayList<>();
        for (MStats.MStatusDatePair p : pairs) {
            labels.add(YEAR_MONTH.format(p.getFrom()));
        }

        List<MStats.MStatusMonthlyData> msmd = new ArrayList<>();
        List<Long> data = new ArrayList<>();
        for (MStats.MStatusDatePair p : pairs) {
            data.add(presaleStatusRepository.countByShopAndStatusAndCreatedBetween(shop, Status.PENDING, p.getFrom(), p.getTo()));
        }
        msmd.add(new MStats.MStatusMonthlyData(data, "Pendientes"));
        data = new ArrayList<>();
        for (MStats.MStatusDatePair p : pairs) {
            data.add(presaleStatusRepository.countByShopAndStatusAndCreatedBetween(shop, Status.SENT, p.getFrom(), p.getTo()));
        }
        msmd.add(new MStats.MStatusMonthlyData(data, "Enviados"));
        data = new ArrayList<>();
        for (MStats.MStatusDatePair p : pairs) {
            data.add(presaleStatusRepository.countByShopAndStatusAndCreatedBetween(shop, Status.DELIVER, p.getFrom(), p.getTo()));
        }
        msmd.add(new MStats.MStatusMonthlyData(data, "Entregados"));
        data = new ArrayList<>();
        for (MStats.MStatusDatePair p : pairs) {
            data.add(presaleStatusRepository.countByShopAndStatusAndCreatedBetween(shop, Status.CANCEL, p.getFrom(), p.getTo()));
        }
        msmd.add(new MStats.MStatusMonthlyData(data, "Cancelados"));

        return new MStats.MStatusMonthly(msmd, labels);
    }

    private List<MStats.MStatusDatePair> getDatePair() {
        List<MStats.MStatusDatePair> pairs = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_MONTH, 1);
            trimTime(c);
            c.add(Calendar.MONTH, (11 - i) * -1);
            Date from = c.getTime();
            c.add(Calendar.MONTH, 1);
            Date to = c.getTime();
            pairs.add(new MStats.MStatusDatePair(from, to));
        }
        return pairs;
    }

    private MStats.MStatsStatusSingle getStatusSingle(Shop shop, Status status, Date from, Date fromYear, Date to) {
        return new MStats.MStatsStatusSingle(
                presaleStatusRepository.countByShopAndStatusAndNextIsNullAndCreatedBetween(shop, status, from, to),
                presaleStatusRepository.countByShopAndStatusAndNextIsNullAndCreatedBetween(shop, status, fromYear, to)
        );
    }

    private void append(Map<Customer, Long> customers, Map<User, Long> users, Presale p) {
        if (p.getCustomer() != null) {
            if (customers.containsKey(p.getCustomer())) {
                Long c = customers.get(p.getCustomer()) + 1;
                customers.put(p.getCustomer(), c);
            } else {
                customers.put(p.getCustomer(), new Long(1));
            }
        }
        if (p.getCreator() != null) {
            if (users.containsKey(p.getCreator())) {
                Long c = users.get(p.getCreator()) + 1;
                users.put(p.getCreator(), c);
            } else {
                users.put(p.getCreator(), new Long(1));
            }
        }
    }

    private void trimTime(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
    }

}
