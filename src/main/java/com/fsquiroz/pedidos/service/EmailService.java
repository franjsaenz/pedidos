package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.controller.AngularController;
import com.fsquiroz.pedidos.entity.db.PasswordToken;
import java.util.Arrays;
import java.util.List;
import javax.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class EmailService {

    @Autowired
    private JavaMailSender sender;

    @Value("${com.fsquiroz.pedidos.service.MailService.from}")
    private String from;

    public void newUser(PasswordToken token) {
        StringBuilder sb = new StringBuilder("Bienvenid@ a Pedidos!<br/>Para activar tu cuenta deberás utilizar <a href= \"");

        StringBuilder url = new StringBuilder();
        url.append(ControllerLinkBuilder.linkTo(AngularController.class).slash("password/reset").slash(token.getId()).withSelfRel().getHref());
        sb.append(url);
        sb.append("\">este elance</a> y podrás configurar tu password");

        send(token.getUser().getEmail(), "Bienvenid@", sb.toString());
    }

    public void resetPassword(PasswordToken token) {
        StringBuilder sb = new StringBuilder("Perdida de password!<br/>Para cambiar tu password deberás seguir <a href= \"");

        StringBuilder url = new StringBuilder();
        url.append(ControllerLinkBuilder.linkTo(AngularController.class).slash("password/reset").slash(token.getId()).withSelfRel().getHref());
        sb.append(url);
        sb.append("\">este elance</a>");

        send(token.getUser().getEmail(), "Password", sb.toString());
    }

    private void send(String to, String subject, String message) {
        send(Arrays.asList(to), subject, message);
    }

    private void send(List<String> to, String subject, String body) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Sending e-mail with subject '{}' to '{}'", subject, to);
                try {
                    MimeMessage message = sender.createMimeMessage();
                    MimeMessageHelper helper = new MimeMessageHelper(message, "UTF-8");
                    helper.setFrom(from, "Pedidos");
                    helper.setTo(to.toArray(new String[]{}));
                    helper.setSubject(subject);
                    helper.setText(body, true);
                    sender.send(message);
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }).start();
    }

}
