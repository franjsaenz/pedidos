package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Category;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MCategory;
import com.fsquiroz.pedidos.exception.AppErrorCode;
import com.fsquiroz.pedidos.exception.BadRequestException;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.repository.CategoryRepository;
import com.fsquiroz.pedidos.repository.SearchRepository;
import org.springframework.stereotype.Service;
import com.fsquiroz.pedidos.service.declaration.ICategoryService;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class CategoryService implements ICategoryService {

    private ValidationService validationService;

    private CategoryRepository categoryRepository;

    private SearchRepository searchRepository;
    
    private EntityBuildService entityBuildService;

    public CategoryService(ValidationService validationService,
            CategoryRepository categoryRepository,
            SearchRepository searchRepository,
            EntityBuildService entityBuildService) {
        this.validationService = validationService;
        this.categoryRepository = categoryRepository;
        this.searchRepository = searchRepository;
        this.entityBuildService = entityBuildService;
    }

    @Override
    public Page<Category> findByParent(Shop shop, Category parent, String name, Pageable page) {
        return searchRepository.searchCategories(shop, name, parent, true, page);
    }

    @Override
    public MCategory build(Category category, boolean withChilds) {
        String parentId = category.getParent() != null ? category.getParent().getId() : null;
        MCategory mc = new MCategory(category.getId(), category.getCode(), category.getName(), null, parentId);

        if (withChilds) {
            Set<MCategory> childs = new LinkedHashSet<>();
            for (Category child : category.getChilds()) {
                MCategory c = build(child, true);
                childs.add(c);
            }
            mc.setChilds(childs);
        }
        entityBuildService.buildEntity(category, mc);
        return mc;
    }

    @Override
    public Category get(String id) {
        return categoryRepository.findById(id).orElseThrow(() -> {
            return new NotFoundException(Category.class);
        });
    }

    @Override
    public Category create(Shop shop, MCategory model, User creator) {
        validationService.notEmpty(model.getCode(), "code");
        validationService.notEmpty(model.getName(), "name");
        Category parent = null;
        if (model.getParentId() != null) {
            parent = get(model.getParentId());
        }
        Category c = new Category();
        c.setCreated(new Date());
        c.setCreator(creator);
        c.setShop(shop);
        c.setCode(model.getCode());
        c.setName(model.getName());
        c.setParent(parent);
        c = categoryRepository.insert(c);
        if (parent != null) {
            parent.getChilds().add(c);
            categoryRepository.save(parent);
        }
        return c;
    }

    @Override
    public Category update(Category c, MCategory model, User editor) {
        validationService.notEmpty(model.getCode(), "code");
        validationService.notEmpty(model.getName(), "name");
        Category newParent = null;
        if (model.getParentId() != null) {
            newParent = get(model.getParentId());
        }

        c.setEdited(new Date());
        c.setEditor(editor);
        c.setCode(model.getCode());
        c.setName(model.getName());

        Category oldParent = c.getParent();

        if (newParent != null && !newParent.equals(oldParent)) {
            newParent.getChilds().add(c);
            categoryRepository.save(newParent);

            if (oldParent != null) {
                oldParent.getChilds().remove(c);
                categoryRepository.save(oldParent);
            }
        }

        return categoryRepository.save(c);
    }

    @Override
    public void delete(Category domain, User deleter) {
        if (!domain.getChilds().isEmpty()) {
            throw new BadRequestException(AppErrorCode.NOT_EMPTY_CATEGORY, "Categories with childs can not be deleted");
        }
        Category parent = domain.getParent();
        if (parent != null) {
            parent.getChilds().remove(domain);
            categoryRepository.save(parent);
        }
        domain.setDeleted(new Date());
        domain.setDeleter(deleter);
        categoryRepository.save(domain);
    }

    @Override
    public Page<Category> list(Shop shop, String name, Pageable page) {
        return searchRepository.searchCategories(shop, name, null, false, page);
    }

}
