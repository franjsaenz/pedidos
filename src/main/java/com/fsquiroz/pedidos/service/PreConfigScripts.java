package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Role;
import com.fsquiroz.pedidos.entity.json.MUser;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.service.declaration.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PreConfigScripts implements ApplicationRunner {

    private IUserService userService;

    public PreConfigScripts(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        createSuperUser();
    }

    private void createSuperUser() {
        try {
            userService.getByEmail(null, "super@pedidos.com");
        } catch (NotFoundException nfe) {
            log.info("Creating super user...");
            MUser mu = new MUser(null, "super@pedidos.com", "User", "Super", Role.SUPER, null, "123456");
            userService.create(null, mu, null);
        }
    }

}
