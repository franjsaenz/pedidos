package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Entity;
import com.fsquiroz.pedidos.entity.db.PasswordToken;
import com.fsquiroz.pedidos.entity.db.Role;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MEntity;
import com.fsquiroz.pedidos.entity.json.MLogin;
import com.fsquiroz.pedidos.entity.json.MUpdatePassword;
import com.fsquiroz.pedidos.entity.json.MUser;
import com.fsquiroz.pedidos.exception.AppErrorCode;
import com.fsquiroz.pedidos.exception.BadRequestException;
import com.fsquiroz.pedidos.exception.MultiShopException;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.exception.UnauthorizedException;
import com.fsquiroz.pedidos.repository.PasswordTokenRepository;
import com.fsquiroz.pedidos.repository.SearchRepository;
import com.fsquiroz.pedidos.repository.UserRepository;
import com.fsquiroz.pedidos.security.SecurityCheck;
import com.fsquiroz.pedidos.service.declaration.IUserService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

    private PasswordEncoder passwordEncoder;

    private SecurityCheck securityCheck;

    private ValidationService validationService;

    private EmailService emailService;

    private UserRepository userRepository;

    private PasswordTokenRepository passwordTokenRepository;

    private SearchRepository searchRepository;

    public UserService(PasswordEncoder passwordEncoder,
            SecurityCheck securityCheck,
            ValidationService validationService,
            EmailService emailService,
            UserRepository userRepository,
            PasswordTokenRepository passwordTokenRepository,
            SearchRepository searchRepository) {
        this.passwordEncoder = passwordEncoder;
        this.securityCheck = securityCheck;
        this.validationService = validationService;
        this.emailService = emailService;
        this.userRepository = userRepository;
        this.passwordTokenRepository = passwordTokenRepository;
        this.searchRepository = searchRepository;
    }

    @Override
    public User login(Shop shop, MLogin login) {
        validationService.notEmpty(login.getEmail(), "email");
        validationService.notEmpty(login.getPassword(), "password");
        User u;
        if (shop == null) {
            u = getUser(login.getEmail());
        } else {
            u = getUser(shop, login.getEmail());
        }
        if (!passwordEncoder.matches(login.getPassword(), u.getPassword())) {
            throw new UnauthorizedException(AppErrorCode.INVALID_EMAIL_PASSWORD, "Invalidad email or password");
        }
        String token = securityCheck.generateToken(u);
        u.setToken(token);
        return u;
    }

    private User getUser(Shop shop, String email) {
        return userRepository.findOneByShopAndEmail(shop, email).orElseThrow(() -> {
            return new UnauthorizedException(AppErrorCode.INVALID_EMAIL_PASSWORD, "Invalidad email or password");
        });
    }

    private User getUser(String email) {
        List<User> users = userRepository.findByEmail(email);
        if (users.isEmpty()) {
            throw new UnauthorizedException(AppErrorCode.INVALID_EMAIL_PASSWORD, "Invalidad email or password");
        } else if (users.size() == 1) {
            return users.get(0);
        } else {
            List<Shop> shops = new ArrayList<>();
            for (User user : users) {
                shops.add(user.getShop());
            }
            throw new MultiShopException(shops);
        }
    }

    @Override
    public void updatePassword(User user, MUpdatePassword password) {
        validationService.notEmpty(password.getOldPassword(), "oldPassword");
        validationService.notEmpty(password.getNewPassword(), "newPassword");
        if (!passwordEncoder.matches(password.getOldPassword(), user.getPassword())) {
            throw new UnauthorizedException(AppErrorCode.MISSMATCH_OLD_PASSWORD, "Your inputed old password does not match your current password.");
        }
        String encoded = passwordEncoder.encode(password.getNewPassword());
        List<User> users = userRepository.findByEmail(user.getEmail());
        for (User u : users) {
            u.setPassword(encoded);
        }
        userRepository.saveAll(users);
    }

    @Override
    public void updatePassword(MUpdatePassword password) {
        validationService.notEmpty(password.getResetToken(), "resetToken");
        validationService.notEmpty(password.getNewPassword(), "newPassword");
        PasswordToken pt = passwordTokenRepository.findById(password.getResetToken()).orElseThrow(() -> {
            return new NotFoundException(PasswordToken.class);
        });
        if (pt.getDeleted() != null) {
            throw new BadRequestException(AppErrorCode.ALREADY_USED_TOKEN, "This token has already been used.");
        }
        List<User> users = userRepository.findByEmail(pt.getUser().getEmail());
        String encoded = passwordEncoder.encode(password.getNewPassword());
        for (User user : users) {
            user.setPassword(encoded);
        }
        userRepository.saveAll(users);
        pt.setDeleted(new Date());
        passwordTokenRepository.save(pt);
    }

    @Override
    public void resetPassword(String email) {
        validationService.notEmpty(email, "email");
        User user;
        List<User> users = userRepository.findByEmail(email);
        if (users.isEmpty()) {
            throw new NotFoundException(User.class);
        } else {
            user = users.get(0);
        }
        PasswordToken pt = new PasswordToken();
        pt.setCreated(new Date());
        pt.setUser(user);
        pt = passwordTokenRepository.insert(pt);
        emailService.resetPassword(pt);
    }

    @Override
    public MUser build(User domain) {
        MUser mu = new MUser(domain.getId(), domain.getEmail(), domain.getFirstName(), domain.getLastName(), domain.getRole(), domain.getToken(), null);
        buildEntity(domain, mu);
        return mu;
    }

    private void buildEntity(Entity e, MEntity me) {
        me.setCreated(e.getCreated());
        me.setDeleted(me.getDeleted());
        me.setEdited(me.getEdited());
        if (e.getCreator() != null) {
            me.setCreator(this.build(e.getCreator()));
        }
        if (e.getEditor() != null) {
            me.setEditor(this.build(e.getEditor()));
        }
        if (e.getDeleter() != null) {
            me.setDeleter(this.build(e.getDeleter()));
        }
    }

    @Override
    public User get(String id) {
        return userRepository.findById(id).orElseThrow(() -> {
            return new NotFoundException(User.class);
        });
    }

    @Override
    public User getByEmail(Shop shop, String email) {
        return userRepository.findOneByShopAndEmail(shop, email).orElseThrow(() -> {
            return new NotFoundException(User.class);
        });
    }

    @Override
    public User create(Shop shop, MUser model, User creator) {
        validationService.notEmpty(model.getEmail(), "email");
        if (shop != null && "super@pedidos.com".equals(model.getEmail())) {
            throw new BadRequestException(AppErrorCode.ALREADY_USED_EMAIL, "Email 'super@pedidos.com' can not be used as regular user");
        }
        User u = userRepository.findOneByShopAndEmail(shop, model.getEmail()).orElse(null);
        if (u != null) {
            throw new BadRequestException(AppErrorCode.ALREADY_USED_EMAIL, "Email '" + model.getEmail() + "' already in use for other user");
        } else {
            u = new User();
        }
        String oldPassword = null;
        List<User> users = userRepository.findByEmail(model.getEmail());
        if (users.size() > 0) {
            oldPassword = users.get(0).getPassword();
        }
        u.setCreated(new Date());
        u.setCreator(creator);
        u.setShop(shop);
        u.setEmail(model.getEmail());
        u.setFirstName(model.getFirstName());
        u.setLastName(model.getLastName());
        if (shop != null && model.getRole() == Role.SUPER) {
            throw new BadRequestException(AppErrorCode.RESERVED_ROLE, "'SUPER' role is reserved to platform owner");
        }
        u.setRole(model.getRole());
        if (oldPassword != null) {
            u.setPassword(oldPassword);
            return userRepository.insert(u);
        } else if ("super@pedidos.com".equals(model.getEmail())) {
            u.setPassword(passwordEncoder.encode(model.getPassword()));
            return userRepository.insert(u);
        } else {
            u.setPassword(passwordEncoder.encode(UUID.randomUUID().toString()));
        }
        u = userRepository.insert(u);
        PasswordToken pt = new PasswordToken();
        pt.setCreated(new Date());
        pt.setUser(u);
        pt = passwordTokenRepository.insert(pt);
        emailService.newUser(pt);
        return u;
    }

    @Override
    public User update(User original, MUser edit, User creator) {
        original.setFirstName(edit.getFirstName());
        original.setLastName(edit.getLastName());
        original.setRole(edit.getRole());
        original.setEdited(new Date());
        original.setEditor(creator);
        return userRepository.save(original);
    }

    @Override
    public void delete(User domain, User creator) {
        domain.setDeleted(new Date());
        domain.setDeleter(creator);
        userRepository.save(domain);
    }

    @Override
    public Page<User> list(Shop shop, String name, Pageable page) {
        return searchRepository.searchUsers(shop, name, page);
    }

}
