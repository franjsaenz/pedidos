package com.fsquiroz.pedidos.service.declaration;

import com.fsquiroz.pedidos.entity.db.Address;
import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.json.MCustomer;
import java.util.List;

public interface ICustomerService extends IBuildableService<Customer, MCustomer> {

    public void setAddress(Customer customer, Address address);

    public void removeAddress(Customer customer);

    public List<Customer> getSync(Shop shop);

}
