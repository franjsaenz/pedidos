package com.fsquiroz.pedidos.service.declaration;

import com.fsquiroz.pedidos.entity.db.Address;
import com.fsquiroz.pedidos.entity.json.MAddress;

public interface IAddressService extends IBuildableService<Address, MAddress> {

}
