package com.fsquiroz.pedidos.service.declaration;

import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MLogin;
import com.fsquiroz.pedidos.entity.json.MUpdatePassword;
import com.fsquiroz.pedidos.entity.json.MUser;

public interface IUserService extends IBuildableService<User, MUser> {

    public User getByEmail(Shop shop, String email);

    public User login(Shop shop, MLogin login);

    public void updatePassword(User user, MUpdatePassword password);

    public void updatePassword(MUpdatePassword password);

    public void resetPassword(String email);

}
