package com.fsquiroz.pedidos.service.declaration;

import com.fsquiroz.pedidos.entity.db.Category;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.json.MCategory;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public interface ICategoryService extends ICrudService<Category, MCategory> {

    public Page<Category> findByParent(Shop shop, Category parent, String name, Pageable page);

    public MCategory build(Category category, boolean withChilds);

    public default Page<MCategory> build(Page<Category> categories, boolean withChilds) {
        List<MCategory> models = new ArrayList<>();
        for (Category c : categories) {
            models.add(build(c, withChilds));
        }
        return new PageImpl<>(models, categories.getPageable(), categories.getTotalElements());
    }

}
