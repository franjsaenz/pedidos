package com.fsquiroz.pedidos.service.declaration;

import com.fsquiroz.pedidos.entity.db.Category;
import com.fsquiroz.pedidos.entity.db.Photo;
import com.fsquiroz.pedidos.entity.db.Product;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MProduct;
import java.util.Collection;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IProductService extends IBuildableService<Product, MProduct> {

    public void addPhoto(User editor, Product product, Photo photo);

    public void delPhoto(User deleter, Product product);

    public Page<Product> listByCategory(Shop shop, String name, Collection<Category> categories, Pageable page);

    public List<Product> getSync(Shop shop);

}
