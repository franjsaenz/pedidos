package com.fsquiroz.pedidos.service.declaration;

import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ICrudService<D, M> {

    public D get(String id);

    public D create(Shop shop, M model, User creator);

    public D update(D original, M edit, User editor);

    public void delete(D domain, User deleter);

    public Page<D> list(Shop shop, String name, Pageable page);

}
