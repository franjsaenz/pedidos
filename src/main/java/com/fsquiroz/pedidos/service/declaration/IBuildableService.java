package com.fsquiroz.pedidos.service.declaration;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public interface IBuildableService<D, M> extends ICrudService<D, M> {

    public M build(D domain);

    public default Page<M> build(Page<D> domains) {
        List<M> models = new ArrayList<>();
        for (D d : domains) {
            models.add(build(d));
        }
        return new PageImpl<>(models, domains.getPageable(), domains.getTotalElements());
    }

}
