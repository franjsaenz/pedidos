package com.fsquiroz.pedidos.service.declaration;

import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Detail;
import com.fsquiroz.pedidos.entity.db.Presale;
import com.fsquiroz.pedidos.entity.db.Product;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.Status;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MAsyncPresale;
import com.fsquiroz.pedidos.entity.json.MDetail;
import com.fsquiroz.pedidos.entity.json.MPresale;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public interface IPresaleService extends IBuildableService<Presale, MPresale> {

    public Presale get(Shop shop, long friendlyNumber);

    public Detail getDetail(String id);

    public Detail add(Presale presale, MDetail detail, User creator);

    public Detail update(Presale presale, Detail original, MDetail edit, User editor);

    public void remove(Presale presale, Detail detail, User deleter);
    
    public void createAsync(MAsyncPresale map, User creator);

    public Page<Presale> filter(Shop shop, Collection<Status> statuses, Customer customer, Pageable page);

    public Page<Detail> listDetails(Shop shop, Presale presale, Pageable page);

    public Page<Detail> listDetails(Shop shop, Product product, Pageable page);

    public Page<Detail> listDetails(Shop shop, Customer customer, Pageable page);

    public MDetail buildDetail(Detail detail);

    public default Page<MDetail> buildDetails(Page<Detail> domains) {
        List<MDetail> models = new ArrayList<>();
        for (Detail d : domains) {
            models.add(buildDetail(d));
        }
        return new PageImpl<>(models, domains.getPageable(), domains.getTotalElements());
    }

}
