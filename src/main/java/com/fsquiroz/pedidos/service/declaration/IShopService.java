package com.fsquiroz.pedidos.service.declaration;

import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MShop;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public interface IShopService {

    public Shop get(String id);

    public Shop create(MShop model, User creator);

    public Shop update(Shop original, MShop edit, User creator);

    public void delete(Shop shop, User creator);

    public Page<Shop> list(String name, Pageable page);

    public MShop build(Shop shop);

    public default Page<MShop> build(Page<Shop> domains) {
        List<MShop> models = build(domains.getContent());
        return new PageImpl<>(models, domains.getPageable(), domains.getTotalElements());
    }

    public default List<MShop> build(List<Shop> domains) {
        List<MShop> models = new ArrayList<>();
        for (Shop d : domains) {
            models.add(build(d));
        }
        return models;
    }

}
