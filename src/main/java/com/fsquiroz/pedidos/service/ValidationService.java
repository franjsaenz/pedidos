package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.exception.AppErrorCode;
import com.fsquiroz.pedidos.exception.BadRequestException;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {

    public void notNull(Object o, String field) {
        if (o == null) {
            throw new BadRequestException(AppErrorCode.MISSING_FIELD, "'" + field + "' can not be empty");
        }
    }

    public void notEmpty(String s, String field) {
        notNull(s, field);
        if (s.isEmpty()) {
            throw new BadRequestException(AppErrorCode.MISSING_FIELD, "'" + field + "' can not be empty");
        }
    }

}
