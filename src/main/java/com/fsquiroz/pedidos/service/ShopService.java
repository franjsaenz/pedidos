package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Role;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MShop;
import com.fsquiroz.pedidos.entity.json.MUser;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.repository.SearchRepository;
import com.fsquiroz.pedidos.repository.ShopRepository;
import com.fsquiroz.pedidos.service.declaration.IShopService;
import com.fsquiroz.pedidos.service.declaration.IUserService;
import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ShopService implements IShopService {

    private ValidationService validationService;

    private IUserService userService;

    private ShopRepository shopRepository;

    private SearchRepository searchRepository;
    
    private EntityBuildService entityBuildService;

    public ShopService(ValidationService validationService,
            IUserService userService,
            ShopRepository shopRepository,
            SearchRepository searchRepository,
            EntityBuildService entityBuildService) {
        this.validationService = validationService;
        this.userService = userService;
        this.shopRepository = shopRepository;
        this.searchRepository = searchRepository;
        this.entityBuildService = entityBuildService;
    }

    @Override
    public Shop get(String id) {
        return shopRepository.findById(id).orElseThrow(() -> {
            return new NotFoundException(Shop.class);
        });
    }

    @Override
    public Shop create(MShop model, User creator) {
        validationService.notEmpty(model.getUser().getEmail(), "email");
        Shop s = new Shop();
        s.setCreated(new Date());
        s.setCreator(creator);
        s.setName(model.getName());
        s = shopRepository.insert(s);
        MUser u = model.getUser();
        u.setRole(Role.ADMIN);
        userService.create(s, u, creator);
        return s;
    }

    @Override
    public Shop update(Shop original, MShop edit, User editor) {
        original.setName(edit.getName());
        original.setEdited(new Date());
        original.setEditor(editor);
        return shopRepository.save(original);
    }

    @Override
    public void delete(Shop shop, User deleter) {
        shop.setDeleted(new Date());
        shop.setDeleter(deleter);
        shopRepository.save(shop);
    }

    @Override
    public Page<Shop> list(String name, Pageable page) {
        return searchRepository.searchShops(name, page);
    }

    @Override
    public MShop build(Shop shop) {
        MShop ms = new MShop(shop.getId(), shop.getName(), shop.getFriendlyName(), null);
        entityBuildService.buildEntity(shop, ms);
        return ms;
    }

}
