package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Category;
import com.fsquiroz.pedidos.entity.db.Photo;
import com.fsquiroz.pedidos.entity.db.Product;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MCategory;
import com.fsquiroz.pedidos.entity.json.MProduct;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.repository.ProductRepository;
import com.fsquiroz.pedidos.repository.SearchRepository;
import com.fsquiroz.pedidos.service.declaration.ICategoryService;
import com.fsquiroz.pedidos.service.declaration.IProductService;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductService implements IProductService {

    private ICategoryService categoryService;

    private ValidationService validationService;

    private ProductRepository productRepository;

    private SearchRepository searchRepository;

    private EntityBuildService entityBuildService;

    private PhotoService photoService;

    public ProductService(ICategoryService categoryService,
            ValidationService validationService,
            ProductRepository productRepository,
            SearchRepository searchRepository,
            EntityBuildService entityBuildService,
            PhotoService photoService) {
        this.categoryService = categoryService;
        this.validationService = validationService;
        this.productRepository = productRepository;
        this.searchRepository = searchRepository;
        this.entityBuildService = entityBuildService;
        this.photoService = photoService;
    }

    @Override
    public Page<Product> listByCategory(Shop shop, String name, Collection<Category> categories, Pageable page) {
        return searchRepository.searchProducts(shop, name, categories, page);
    }

    @Override
    public MProduct build(Product domain) {
        MCategory mc = null;
        if (domain.getCategory() != null) {
            mc = categoryService.build(domain.getCategory(), false);
        }
        String catId = mc != null ? mc.getId() : null;
        String photo = photoService.getUrl(domain.getPhoto());
        MProduct mp = new MProduct(
                domain.getId(),
                domain.getName(),
                domain.getCode(),
                domain.getPurchasePrice(),
                domain.getSalePrice(),
                domain.getTax(),
                mc,
                catId,
                photo
        );
        entityBuildService.buildEntity(domain, mp);
        return mp;
    }

    @Override
    public Product get(String id) {
        return productRepository.findById(id).orElseThrow(() -> {
            return new NotFoundException(Product.class);
        });
    }

    @Override
    public Product create(Shop shop, MProduct model, User creator) {
        validationService.notEmpty(model.getCode(), "code");
        validationService.notEmpty(model.getName(), "name");
        Category c = null;
        if (model.getCategoryId() != null) {
            c = categoryService.get(model.getCategoryId());
        }
        Product p = new Product();
        p.setCreated(new Date());
        p.setCreator(creator);
        p.setShop(shop);
        p.setCategory(c);
        p.setCode(model.getCode());
        p.setName(model.getName());
        p.setPurchasePrice(model.getPurchasePrice());
        p.setSalePrice(model.getSalePrice());
        p.setTax(model.getTax());
        return productRepository.insert(p);
    }

    @Override
    public Product update(Product original, MProduct edit, User editor) {
        validationService.notEmpty(edit.getCode(), "code");
        validationService.notEmpty(edit.getName(), "name");
        Category c = null;
        if (edit.getCategoryId() != null) {
            c = categoryService.get(edit.getCategoryId());
        }
        original.setEdited(new Date());
        original.setEditor(editor);
        original.setCategory(c);
        original.setCode(edit.getCode());
        original.setName(edit.getName());
        original.setPurchasePrice(edit.getPurchasePrice());
        original.setSalePrice(edit.getSalePrice());
        original.setTax(edit.getTax());
        return productRepository.save(original);
    }

    @Override
    public void delete(Product domain, User deleter) {
        domain.setDeleted(new Date());
        domain.setDeleter(deleter);
        productRepository.save(domain);
    }

    @Override
    public Page<Product> list(Shop shop, String name, Pageable page) {
        return listByCategory(shop, name, null, page);
    }

    @Override
    public List<Product> getSync(Shop shop) {
        return productRepository.findByShopAndDeletedIsNull(shop);
    }

    @Override
    public void addPhoto(User editor, Product product, Photo photo) {
        product.setPhoto(photo);
        product.setEdited(new Date());
        product.setEditor(editor);
        productRepository.save(product);
    }

    @Override
    public void delPhoto(User deleter, Product product) {
        product.setPhoto(null);
        product.setEdited(new Date());
        product.setEditor(deleter);
        productRepository.save(product);
    }

}
