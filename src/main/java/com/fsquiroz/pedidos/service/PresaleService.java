package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Address;
import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Detail;
import com.fsquiroz.pedidos.entity.db.Presale;
import com.fsquiroz.pedidos.entity.db.PresaleStatus;
import com.fsquiroz.pedidos.entity.db.Product;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.Status;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MAddress;
import com.fsquiroz.pedidos.entity.json.MAsyncPresale;
import com.fsquiroz.pedidos.entity.json.MCustomer;
import com.fsquiroz.pedidos.entity.json.MDetail;
import com.fsquiroz.pedidos.entity.json.MPresale;
import com.fsquiroz.pedidos.entity.json.MProduct;
import com.fsquiroz.pedidos.entity.json.MUser;
import com.fsquiroz.pedidos.exception.AppErrorCode;
import com.fsquiroz.pedidos.exception.BadRequestException;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.repository.CustomerRepository;
import com.fsquiroz.pedidos.repository.DetailRepository;
import com.fsquiroz.pedidos.repository.PresaleRepository;
import com.fsquiroz.pedidos.repository.PresaleStatusRepository;
import com.fsquiroz.pedidos.repository.SearchRepository;
import com.fsquiroz.pedidos.service.declaration.IAddressService;
import com.fsquiroz.pedidos.service.declaration.ICustomerService;
import com.fsquiroz.pedidos.service.declaration.IPresaleService;
import com.fsquiroz.pedidos.service.declaration.IProductService;
import com.fsquiroz.pedidos.service.declaration.IUserService;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PresaleService implements IPresaleService {

    private IAddressService addressService;

    private ICustomerService customerService;

    private IUserService userService;

    private IProductService productService;

    private ValidationService validationService;

    private PresaleRepository presaleRepository;

    private PresaleStatusRepository presaleStatusRepository;

    private DetailRepository detailRepository;

    private CustomerRepository customerRepository;

    private SearchRepository searchRepository;

    private EntityBuildService entityBuildService;

    public PresaleService(IAddressService addressService,
            ICustomerService customerService,
            IUserService userService,
            IProductService productService,
            ValidationService validationService,
            PresaleRepository presaleRepository,
            PresaleStatusRepository presaleStatusRepository,
            DetailRepository detailRepository,
            CustomerRepository customerRepository,
            SearchRepository searchRepository,
            EntityBuildService entityBuildService) {
        this.addressService = addressService;
        this.customerService = customerService;
        this.userService = userService;
        this.productService = productService;
        this.validationService = validationService;
        this.presaleRepository = presaleRepository;
        this.presaleStatusRepository = presaleStatusRepository;
        this.detailRepository = detailRepository;
        this.customerRepository = customerRepository;
        this.searchRepository = searchRepository;
        this.entityBuildService = entityBuildService;
    }

    @Override
    public Presale get(Shop shop, long friendlyNumber) {
        return presaleRepository.findOneByShopAndFriendlyNumber(shop, friendlyNumber).orElseThrow(() -> {
            return new NotFoundException(Presale.class);
        });
    }

    @Override
    public Detail getDetail(String id) {
        return detailRepository.findById(id).orElseThrow(() -> {
            return new NotFoundException(Detail.class);
        });
    }

    @Override
    public Detail add(Presale presale, MDetail detail, User creator) {
        validationService.notEmpty(detail.getProductId(), "productId");
        Product p = productService.get(detail.getProductId());
        Detail d = new Detail();
        d.setCreated(new Date());
        d.setCreator(creator);
        d.setShop(presale.getShop());
        d.setAmount(detail.getAmount());
        d.setCustomer(presale.getCustomer());
        d.setDiscount(detail.getDiscount());
        d.setPresale(presale);
        d.setProduct(p);
        d.setCreated(new Date());
        calculateSubtotal(d);
        updatablePresale(presale, d, creator);
        d = detailRepository.insert(d);
        calculateBalance(presale.getCustomer());
        return d;
    }

    @Override
    public Detail update(Presale presale, Detail original, MDetail edit, User editor) {
        updatablePresale(presale, original, editor);
        validationService.notEmpty(edit.getProductId(), "productId");
        Product p = productService.get(edit.getProductId());
        original.setEdited(new Date());
        original.setEditor(editor);
        original.setAmount(edit.getAmount());
        original.setCustomer(presale.getCustomer());
        original.setDiscount(edit.getDiscount());
        original.setProduct(p);
        original.setEdited(new Date());
        calculateSubtotal(original);
        original = detailRepository.save(original);
        calculateBalance(presale.getCustomer());
        return original;
    }

    @Override
    public void remove(Presale presale, Detail detail, User deleter) {
        updatablePresale(presale, detail, deleter);
        detail.setDeleted(new Date());
        detail.setDeleter(deleter);
        detailRepository.save(detail);
        calculateBalance(presale.getCustomer());
    }

    @Override
    public void createAsync(MAsyncPresale map, User creator) {
        MPresale mp = map.getPresale();
        MCustomer mc = mp.getCustomer();
        MAddress ma = mp.getAddress();

        validationService.notEmpty(mp.getCustomerId(), "customerId");

        Customer c = customerService.get(mc.getId());
        Address a;
        if (ma.getId().equals(mc.getAddress().getId())) {
            a = c.getAddress();
        } else {
            a = addressService.create(null, ma, creator);
            mp.setAddressId(a.getId());
            mp.getAddress().setId(a.getId());
        }

        Presale p = new Presale();
        p.setCreated(new Date());
        p.setCreator(creator);
        p.setCustomer(c);
        p.setDeliverDate(mp.getDeliverDate());
        p.setDeliveryAddress(a);
        p.setFriendlyNumber(mp.getFriendlyNumber());
        p.setShippingPrice(mp.getShippingPrice());
        p.setStatus(Status.PENDING);
        p.setShop(creator.getShop());
        p.setSubtotal(0);
        p.setTotal(0);

        p = presaleRepository.insert(p);

        getStatus(p, creator);

        List<MDetail> mds = map.getDetails();
        for (MDetail md : mds) {
            add(p, md, creator);
        }

        calculateTotals(p);

        presaleRepository.save(p);

        calculateBalance(p.getCustomer());
    }

    private void calculateBalance(Customer c) {
        double balance = 0;
        List<Presale> preslaes = presaleRepository.findByShopAndDeletedIsNullAndStatusIn(c.getShop(), Arrays.asList(Status.PENDING, Status.SENT));
        for (Presale p : preslaes) {
            calculateTotals(p);
            balance -= p.getTotal();
        }
        c.setBalance(balance);
        customerRepository.save(c);
    }

    private void updatablePresale(Presale presale, Detail detail, User creator) {
        PresaleStatus ps = getStatus(presale, creator);
        Status s = ps.getStatus();
        if (!presale.equals(detail.getPresale())) {
            throw new BadRequestException(AppErrorCode.MISSMATCH_PRESALE, "Selected presale and detail's presale does not match.");
        }
        if (s == Status.DELIVER || s == Status.SENT) {
            throw new BadRequestException(AppErrorCode.SENT_PRESALE, "Selected presale has already been dispatched.");
        }
        if (s == Status.CANCEL) {
            throw new BadRequestException(AppErrorCode.INVALID_PRESALE_STATUS, "Selected presale has been canceled.");
        }
    }

    @Override
    public Page<Presale> filter(Shop shop, Collection<Status> statuses, Customer customer, Pageable page) {
        return this.searchRepository.searchPresales(shop, statuses, customer, page);
    }

    @Override
    public Page<Detail> listDetails(Shop shop, Presale presale, Pageable page) {
        return detailRepository.findByShopAndDeletedIsNullAndPresale(shop, presale, page);
    }

    @Override
    public Page<Detail> listDetails(Shop shop, Product product, Pageable page) {
        return detailRepository.findByShopAndDeletedIsNullAndProduct(shop, product, page);
    }

    @Override
    public Page<Detail> listDetails(Shop shop, Customer customer, Pageable page) {
        return detailRepository.findByShopAndDeletedIsNullAndCustomer(shop, customer, page);
    }

    private PresaleStatus getStatus(Presale p, User creator) {
        PresaleStatus ps = presaleStatusRepository.findFirstByPresaleOrderByCreatedDesc(p);
        if (ps == null) {
            ps = new PresaleStatus();
            ps.setCreated(p.getCreated());
            ps.setCreator(creator);
            ps.setStatus(p.getStatus());
            ps.setPresale(p);
            ps.setShop(p.getShop());
            ps = presaleStatusRepository.insert(ps);
        }
        return ps;
    }

    @Override
    public MPresale build(Presale d) {
        PresaleStatus ps = getStatus(d, null);

        MPresale mp = new MPresale();
        if (d.getDeliveryAddress() != null) {
            MAddress ma = addressService.build(d.getDeliveryAddress());
            mp.setAddress(ma);
            mp.setAddressId(ma.getId());
        }
        if (d.getCustomer() != null) {
            MCustomer mc = customerService.build(d.getCustomer());
            mp.setCustomer(mc);
            mp.setCustomerId(mc.getId());
        }
        mp.setDeliverDate(d.getDeliverDate());
        mp.setDelivered(d.getDelivered());
        mp.setFriendlyNumber(d.getFriendlyNumber());
        mp.setId(d.getId());
        mp.setShippingPrice(d.getShippingPrice());
        mp.setStatus(ps.getStatus());
        mp.setLastStatusUpdate(ps.getCreated());

        if (d.getCreator() != null) {
            MUser mu = userService.build(d.getCreator());
            mp.setUser(mu);
        }
        calculateTotals(d);
        mp.setSubtotal(d.getSubtotal());
        mp.setTotal(d.getTotal());
        entityBuildService.buildEntity(d, mp);
        return mp;
    }

    @Override
    public MDetail buildDetail(Detail d) {
        MDetail md = new MDetail(d.getId(), d.getAmount(), d.getSubtotal(), d.getDiscount(), null, null);
        if (d.getProduct() != null) {
            MProduct mp = productService.build(d.getProduct());
            md.setProduct(mp);
            md.setProductId(mp.getId());
        }
        entityBuildService.buildEntity(d, md);
        return md;
    }

    @Override
    public Presale get(String id) {
        return presaleRepository.findById(id).orElseThrow(() -> {
            return new NotFoundException(Presale.class);
        });
    }

    @Override
    public Presale create(Shop shop, MPresale model, User creator) {
        validationService.notEmpty(model.getCustomerId(), "customerId");
        Customer c = customerService.get(model.getCustomerId());
        Address a;
        if (model.getAddressId() != null) {
            a = addressService.get(model.getAddressId());
        } else {
            a = c.getAddress();
        }
        Presale p = new Presale();
        p.setShop(shop);
        p.setCreated(new Date());
        p.setCreator(creator);
        p.setCustomer(c);
        p.setDeliverDate(model.getDeliverDate());
        p.setDeliveryAddress(a);
        p.setFriendlyNumber(model.getFriendlyNumber());
        p.setShippingPrice(model.getShippingPrice());
        p.setStatus(Status.PENDING);
        p.setSubtotal(0);
        p.setTotal(0);
        p = presaleRepository.insert(p);
        calculateBalance(p.getCustomer());
        getStatus(p, creator);
        return p;
    }

    @Override
    public Presale update(Presale domain, MPresale edit, User editor) {
        PresaleStatus ps = getStatus(domain, editor);
        Address a;
        if (edit.getAddressId() != null) {
            a = addressService.get(edit.getAddressId());
            domain.setDeliveryAddress(a);
        } else if (domain.getCustomer() != null) {
            a = domain.getCustomer().getAddress();
            domain.setDeliveryAddress(a);
        }
        domain.setDeliverDate(edit.getDeliverDate());
        domain.setShippingPrice(edit.getShippingPrice());
        switch (ps.getStatus()) {
            case CANCEL:
                throw new BadRequestException(AppErrorCode.INVALID_PRESALE_STATUS, "Can not change previously canceled presale's status");
            case DELIVER:
                throw new BadRequestException(AppErrorCode.INVALID_PRESALE_STATUS, "Can not change previously delivered presale's status");
            case SENT:
                if (edit.getStatus() == Status.PENDING) {
                    throw new BadRequestException(AppErrorCode.INVALID_PRESALE_STATUS, "Can not change previously sent presale's status to pending");
                }
            case PENDING:
            default:
                if (edit.getStatus() != domain.getStatus()) {
                    PresaleStatus nps = new PresaleStatus();
                    nps.setCreated(new Date());
                    nps.setCreator(editor);
                    nps.setPresale(domain);
                    nps.setStatus(edit.getStatus());
                    nps.setShop(domain.getShop());
                    presaleStatusRepository.insert(nps);
                    ps.setNext(nps);
                    presaleStatusRepository.save(ps);
                    domain.setStatus(edit.getStatus());
                }
                if (edit.getStatus() == Status.DELIVER) {
                    domain.setDelivered(new Date());
                }
                break;
        }
        domain.setEdited(new Date());
        domain.setEditor(editor);
        domain = presaleRepository.save(domain);
        calculateBalance(domain.getCustomer());
        return domain;
    }

    @Override
    public void delete(Presale domain, User deleter) {
        domain.setDeleted(new Date());
        domain.setDeleter(deleter);
        List<Detail> details = detailRepository.findByShopAndDeletedIsNullAndPresale(domain.getShop(), domain);
        for (Detail d : details) {
            d.setDeleted(new Date());
            d.setDeleter(deleter);
        }
        presaleRepository.save(domain);
        detailRepository.saveAll(details);
    }

    @Override
    public Page<Presale> list(Shop shop, String name, Pageable page) {
        return filter(shop, null, null, page);
    }

    private void calculateSubtotal(Detail d) {
        double subtotal = d.getProduct().getSalePrice() - d.getDiscount();
        if (subtotal < 0) {
            subtotal = 0;
        }
        subtotal *= d.getAmount();
        d.setSubtotal(subtotal);
    }

    private void calculateTotals(Presale p) {
        List<Detail> details = detailRepository.findByShopAndDeletedIsNullAndPresale(p.getShop(), p);
        double subtotal = 0;
        for (Detail d : details) {
            calculateSubtotal(d);
            subtotal += d.getSubtotal();
        }
        p.setSubtotal(subtotal);
        double total = subtotal + p.getShippingPrice();
        p.setTotal(total);
    }

}
