package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Entity;
import com.fsquiroz.pedidos.entity.json.MEntity;
import com.fsquiroz.pedidos.service.declaration.IUserService;
import org.springframework.stereotype.Service;

@Service
public class EntityBuildService {

    private IUserService userService;

    public EntityBuildService(IUserService userService) {
        this.userService = userService;
    }

    public void buildEntity(Entity e, MEntity me) {
        me.setCreated(e.getCreated());
        me.setDeleted(me.getDeleted());
        me.setEdited(me.getEdited());
        if (e.getCreator() != null) {
            me.setCreator(userService.build(e.getCreator()));
        }
        if (e.getEditor() != null) {
            me.setEditor(userService.build(e.getEditor()));
        }
        if (e.getDeleter() != null) {
            me.setDeleter(userService.build(e.getDeleter()));
        }
    }

}
