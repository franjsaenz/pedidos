package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.controller.PhotoController;
import com.fsquiroz.pedidos.entity.db.Entity;
import com.fsquiroz.pedidos.entity.db.Photo;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MPhoto;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.repository.PhotoRepository;
import java.io.File;
import java.nio.file.Files;
import java.util.Base64;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Service;

@Service
public class PhotoService {

    @Autowired
    private PhotoRepository photoRepository;

    @Value("${com.fsquiroz.pedidos.service.PhotoService.storage}")
    private String path;

    public Photo create(User user, MPhoto mp, Entity entity) {
        byte[] bin = Base64.getDecoder().decode(mp.getBase64());
        Photo p = new Photo();
        p.setCreated(new Date());
        p.setCreator(user);
        p.setShop(user.getShop());
        p.setMeta(mp.getMeta());
        p.setEntity(entity);
        p.setName(mp.getName());
        p = photoRepository.insert(p);
        save(p, bin);
        return p;
    }

    public Photo get(String id) {
        return photoRepository.findById(id).orElseThrow(() -> {
            return new NotFoundException(Photo.class);
        });
    }

    public byte[] getBin(String id) {
        File f = new File(path + id);
        if (!f.exists()) {
            throw new NotFoundException(Photo.class);
        }
        try {
            return Files.readAllBytes(f.toPath());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public MPhoto getBin(Photo p) {
        MPhoto mp = new MPhoto();
        mp.setBase64(Base64.getEncoder().encodeToString(getBin(p.getId())));
        mp.setName(p.getName());
        mp.setMeta(p.getMeta());
        return mp;
    }

    public String getUrl(Photo p) {
        return p == null ? null : ControllerLinkBuilder.linkTo(PhotoController.class).slash(p.getId()).withSelfRel().getHref();
    }

    private void save(Photo p, byte[] bin) {
        try {
            File folder = new File(path);
            if (!folder.exists()) {
                folder.mkdir();
            }
            File f = new File(path + p.getId());

            if (f.exists()) {
                f.delete();
            }

            Files.write(f.toPath(), bin);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
