package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Address;
import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MAddress;
import com.fsquiroz.pedidos.entity.json.MCustomer;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.repository.CustomerRepository;
import com.fsquiroz.pedidos.repository.SearchRepository;
import com.fsquiroz.pedidos.service.declaration.IAddressService;
import com.fsquiroz.pedidos.service.declaration.ICustomerService;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CustomerService implements ICustomerService {

    private IAddressService addressService;

    private ValidationService validationService;

    private CustomerRepository customerRepository;

    private SearchRepository searchRepository;

    private EntityBuildService entityBuildService;

    public CustomerService(IAddressService addressService,
            ValidationService validationService,
            CustomerRepository customerRepository,
            SearchRepository searchRepository,
            EntityBuildService entityBuildService) {
        this.addressService = addressService;
        this.validationService = validationService;
        this.customerRepository = customerRepository;
        this.searchRepository = searchRepository;
        this.entityBuildService = entityBuildService;
    }

    @Override
    public void setAddress(Customer customer, Address address) {
        customer.setAddress(address);
        customerRepository.save(customer);
    }

    @Override
    public void removeAddress(Customer customer) {
        customer.setAddress(null);
        customerRepository.save(customer);
    }

    @Override
    public MCustomer build(Customer domain) {
        MAddress ma = null;
        if (domain.getAddress() != null) {
            ma = addressService.build(domain.getAddress());
        }
        MCustomer mc = new MCustomer(domain.getId(), domain.getName(), domain.getCuit(), domain.getBalance(), ma, null);
        entityBuildService.buildEntity(domain, mc);
        return mc;
    }

    @Override
    public Customer get(String id) {
        return customerRepository.findById(id).orElseThrow(() -> {
            return new NotFoundException(Customer.class);
        });
    }

    @Override
    public Customer create(Shop shop, MCustomer model, User creator) {
        validationService.notEmpty(model.getCuit(), "cuit");
        validationService.notEmpty(model.getName(), "name");
        Customer c = new Customer();
        c.setCreated(new Date());
        c.setCreator(creator);
        c.setShop(shop);
        c.setCuit(model.getCuit());
        c.setName(model.getName());
        Address a = null;
        if (model.getAddressId() != null) {
            a = addressService.get(model.getAddressId());
        }
        c.setAddress(a);
        return customerRepository.insert(c);
    }

    @Override
    public Customer update(Customer original, MCustomer edit, User editor) {
        validationService.notEmpty(edit.getCuit(), "cuit");
        validationService.notEmpty(edit.getName(), "name");
        original.setEdited(new Date());
        original.setEditor(editor);
        original.setCuit(edit.getCuit());
        original.setName(edit.getName());
        return customerRepository.save(original);
    }

    @Override
    public void delete(Customer domain, User deleter) {
        domain.setDeleted(new Date());
        domain.setDeleter(deleter);
        customerRepository.save(domain);
    }

    @Override
    public Page<Customer> list(Shop shop, String name, Pageable page) {
        return searchRepository.searchCustomer(shop, name, page);
    }

    @Override
    public List<Customer> getSync(Shop shop) {
        return customerRepository.findByShopAndDeletedIsNull(shop);
    }

}
