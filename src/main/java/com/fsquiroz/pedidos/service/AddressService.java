package com.fsquiroz.pedidos.service;

import com.fsquiroz.pedidos.entity.db.Address;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MAddress;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.repository.AddressRepository;
import com.fsquiroz.pedidos.service.declaration.IAddressService;
import java.util.Date;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;

@Service
public class AddressService implements IAddressService {

    private ValidationService validationService;

    private AddressRepository addressRepository;
    
    private EntityBuildService entityBuildService;

    public AddressService(ValidationService validationService,
            AddressRepository addressRepository,
            EntityBuildService entityBuildService) {
        this.validationService = validationService;
        this.addressRepository = addressRepository;
        this.entityBuildService = entityBuildService;
    }

    @Override
    public MAddress build(Address domain) {
        MAddress ma = new MAddress(domain.getId(), domain.getStreet(), domain.getNumber(), domain.getDistrict(), null, null);
        if (domain.getPoint() != null) {
            ma.setLongitude(domain.getPoint().getX());
            ma.setLatitude(domain.getPoint().getY());
        }
        entityBuildService.buildEntity(domain, ma);
        return ma;
    }

    @Override
    public Address get(String id) {
        return addressRepository.findById(id).orElseThrow(() -> {
            return new NotFoundException(Address.class);
        });
    }

    @Override
    public Address create(Shop shop, MAddress model, User creator) {
        validationService.notEmpty(model.getStreet(), "street");
        validationService.notEmpty(model.getDistrict(), "district");
        Address a = new Address();
        a.setCreated(new Date());
        a.setCreator(creator);
        a.setDistrict(model.getDistrict());
        a.setNumber(model.getNumber());
        a.setStreet(model.getStreet());
        if (model.getLatitude() != null && model.getLongitude() != null) {
            GeoJsonPoint point = new GeoJsonPoint(model.getLongitude(), model.getLatitude());
            a.setPoint(point);
        }
        return addressRepository.insert(a);
    }

    @Override
    public Address update(Address original, MAddress edit, User editor) {
        validationService.notNull(edit.getNumber(), "number");
        validationService.notEmpty(edit.getStreet(), "street");
        validationService.notEmpty(edit.getDistrict(), "district");
        original.setEdited(new Date());
        original.setEditor(editor);
        original.setDistrict(edit.getDistrict());
        original.setNumber(edit.getNumber());
        original.setStreet(edit.getStreet());
        if (edit.getLatitude() != null && edit.getLongitude() != null) {
            GeoJsonPoint point = new GeoJsonPoint(edit.getLongitude(), edit.getLatitude());
            original.setPoint(point);
        } else {
            original.setPoint(null);
        }
        return addressRepository.save(original);
    }

    @Override
    public void delete(Address domain, User deleter) {
        domain.setDeleted(new Date());
        domain.setDeleter(deleter);
        addressRepository.save(domain);
    }

    @Override
    public Page<Address> list(Shop shop, String name, Pageable page) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
