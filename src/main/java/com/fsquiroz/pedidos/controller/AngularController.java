package com.fsquiroz.pedidos.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/d")
public class AngularController {

    private static final String FOWARD = "forward:/d/index.html";

    @GetMapping(path = {"/**/{[path:[^\\.]*}", ""})
    public String index() {
        return FOWARD;
    }

}
