package com.fsquiroz.pedidos.controller;

import com.fsquiroz.pedidos.entity.db.Photo;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.service.PhotoService;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/p")
public class PhotoController {

    private PhotoService photoService;

    public PhotoController(PhotoService photoService) {
        this.photoService = photoService;
    }

    @GetMapping("{id}")
    public void get(@PathVariable String id, HttpServletResponse response) {
        try {
            Photo p = photoService.get(id);
            String meta = p.getMeta();
            if (meta != null && meta.contains("gif")) {
                response.setContentType(MediaType.IMAGE_GIF_VALUE);
            } else if (meta != null && meta.contains("png")) {
                response.setContentType(MediaType.IMAGE_PNG_VALUE);
            } else {
                response.setContentType(MediaType.IMAGE_JPEG_VALUE);
            }
            InputStream is = new ByteArrayInputStream(photoService.getBin(id));
            IOUtils.copy(is, response.getOutputStream());
        } catch (NotFoundException nfe) {
            try {
                response.sendError(404, nfe.getMessage());
            } catch (Exception e) {
            }
        } catch (Exception e) {
            try {
                response.sendError(500, e.getMessage());
            } catch (Exception _e) {
            }
        }
    }

}
