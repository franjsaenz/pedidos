package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.json.MException;
import com.fsquiroz.pedidos.entity.json.MMultiShopException;
import com.fsquiroz.pedidos.entity.json.MShop;
import com.fsquiroz.pedidos.exception.AppErrorCode;
import com.fsquiroz.pedidos.exception.AppException;
import com.fsquiroz.pedidos.exception.MultiShopException;
import com.fsquiroz.pedidos.service.declaration.IShopService;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice(annotations = RestController.class)
@Slf4j
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    private IShopService shopService;

    public ApiExceptionHandler(IShopService shopService) {
        this.shopService = shopService;
    }

    @ExceptionHandler(AppException.class)
    public ResponseEntity<MException> appException(HttpServletRequest req, AppException ae) {
        logUrl(req);
        log.debug(ae.getMessage(), ae);
        return parseException(req, ae);
    }

    @ExceptionHandler(ClientAbortException.class)
    public void clientAbortException(HttpServletRequest req, ClientAbortException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<MException> accessDeniedException(HttpServletRequest req, AccessDeniedException e) {
        logUrl(req);
        return build(req, HttpStatus.FORBIDDEN, e);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<MException> genericException(HttpServletRequest req, Exception e) {
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.error("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.error("[{}] {}", req.getMethod(), req.getRequestURL());
        }
        log.error(e.getMessage(), e);
        return parseException(req, e);
    }

    private void logUrl(HttpServletRequest req) {
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.debug("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.debug("[{}] {}", req.getMethod(), req.getRequestURL());
        }
    }

    private ResponseEntity<MException> parseException(HttpServletRequest req, Exception e) {
        ResponseStatus rs = AnnotatedElementUtils.findMergedAnnotation(e.getClass(), ResponseStatus.class);
        HttpStatus status;
        if (rs != null) {
            status = rs.value();
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return build(req, status, e);
    }

    private ResponseEntity<MException> build(HttpServletRequest req, HttpStatus status, Exception e) {
        String message;
        String code;
        List<MShop> shops = null;
        if (e instanceof MultiShopException) {
            message = e.getMessage();
            MultiShopException mse = (MultiShopException) e;
            code = mse.getCode().getNombre();
            shops = shopService.build(mse.getShops());
        } else if (e instanceof AppException) {
            message = e.getMessage();
            code = ((AppException) e).getCode().getNombre();
        } else if (e instanceof AccessDeniedException) {
            message = e.getMessage();
            code = AppErrorCode.ACCESS_DENIED.getNombre();
        } else {
            message = "There has been an unexpected error";
            code = AppErrorCode.INTERNAL_SERVER_ERROR.getNombre();
        }
        MException me;
        if (shops == null) {
            me = new MException(
                    new Date(),
                    status.value(),
                    status.getReasonPhrase(),
                    message,
                    code,
                    req.getRequestURI()
            );
        } else {
            me = new MMultiShopException(
                    new Date(),
                    status.value(),
                    status.getReasonPhrase(),
                    message,
                    code,
                    req.getRequestURI(),
                    shops
            );
        }
        return new ResponseEntity<>(
                me,
                status
        );
    }

}
