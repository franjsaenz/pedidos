package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.db.Address;
import com.fsquiroz.pedidos.entity.json.MAddress;
import com.fsquiroz.pedidos.entity.json.MResponse;
import com.fsquiroz.pedidos.security.SecurityCheck;
import com.fsquiroz.pedidos.service.declaration.IAddressService;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/addresses")
public class AddressApiController {

    private IAddressService addressService;

    private SecurityCheck securityCheck;

    public AddressApiController(IAddressService addressService, SecurityCheck securityCheck) {
        this.addressService = addressService;
        this.securityCheck = securityCheck;
    }

    @PostMapping
    @ApiOperation(
            value = "Create address",
            notes = "Create a new address"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MAddress create(@RequestBody MAddress address) {
        Address a = addressService.create(null, address, securityCheck.getAuthentication());
        return addressService.build(a);
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Get address",
            notes = "Get address by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MAddress get(@PathVariable String id) {
        Address a = addressService.get(id);
        return addressService.build(a);
    }

    @PostMapping("/{id}")
    @ApiOperation(
            value = "Update address",
            notes = "Update address information"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MAddress update(@PathVariable String id, @RequestBody MAddress address) {
        Address a = addressService.get(id);
        a = addressService.update(a, address, securityCheck.getAuthentication());
        return addressService.build(a);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "Delete user",
            notes = "Delete user by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MResponse delete(@PathVariable String id) {
        Address a = addressService.get(id);
        addressService.delete(a, securityCheck.getAuthentication());
        return new MResponse("Address deleted");
    }

}
