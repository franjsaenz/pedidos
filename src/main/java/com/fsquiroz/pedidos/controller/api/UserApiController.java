package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MResponse;
import com.fsquiroz.pedidos.entity.json.MUpdatePassword;
import com.fsquiroz.pedidos.entity.json.MUser;
import com.fsquiroz.pedidos.security.SecurityCheck;
import com.fsquiroz.pedidos.service.declaration.IUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/users")
public class UserApiController {

    private SecurityCheck securityCheck;

    private IUserService userService;

    public UserApiController(SecurityCheck securityCheck, IUserService userService) {
        this.securityCheck = securityCheck;
        this.userService = userService;
    }

    @GetMapping
    @ApiOperation(
            value = "List users",
            notes = "List all users"
    )
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                value = "Page number. Range from 0 to N")
        ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                value = "Page size. Number of elements to be retrieved. Rage from 1 to N")
        ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @PreAuthorize("hasAnyRole('ADMIN')")
    public Page<MUser> list(@ApiIgnore Pageable page, @RequestParam(required = false) String name) {
        Page<User> users = userService.list(securityCheck.getAuthenticationShop(), name, page);
        return userService.build(users);
    }

    @PostMapping
    @ApiOperation(
            value = "Create user",
            notes = "Create a new user"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MUser create(@RequestBody MUser user) {
        User _u = securityCheck.getAuthentication();
        User u = userService.create(_u.getShop(), user, _u);
        return userService.build(u);
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Get user",
            notes = "Get user by id"
    )
    public MUser get(@PathVariable String id) {
        User u = userService.get(id);
        return userService.build(u);
    }

    @PostMapping("/{id}")
    @ApiOperation(
            value = "Update user",
            notes = "Update user information"
    )
    public MUser update(@PathVariable String id, @RequestBody MUser user) {
        User u = userService.get(id);
        u = userService.update(u, user, securityCheck.getAuthentication());
        return userService.build(u);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "Delete user",
            notes = "Delete user by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MResponse delete(@PathVariable String id) {
        User u = userService.get(id);
        userService.delete(u, securityCheck.getAuthentication());
        return new MResponse("User deleted");
    }

    @PostMapping("/password")
    public MResponse updatePassword(@RequestBody MUpdatePassword password) {
        User u = securityCheck.getAuthentication();
        userService.updatePassword(u, password);
        return new MResponse("Password updated");
    }

}
