package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MLogin;
import com.fsquiroz.pedidos.entity.json.MResponse;
import com.fsquiroz.pedidos.entity.json.MUpdatePassword;
import com.fsquiroz.pedidos.entity.json.MUser;
import com.fsquiroz.pedidos.service.declaration.IShopService;
import com.fsquiroz.pedidos.service.declaration.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PublicApiController {

    private IShopService shopService;

    private IUserService userService;

    public PublicApiController(IShopService shopService, IUserService userService) {
        this.shopService = shopService;
        this.userService = userService;
    }

    @PostMapping("/login")
    @ApiOperation(
            value = "User login",
            notes = "Login for users"
    )
    public MUser login(@RequestBody MLogin login, @RequestParam(required = false) String shopId) {
        Shop shop = null;
        if (shopId != null) {
            shop = shopService.get(shopId);
        }
        User u = userService.login(shop, login);
        return userService.build(u);
    }

    @DeleteMapping("/password")
    @ApiOperation(
            value = "Reset user password"
    )
    public MResponse resetPassword(@RequestParam String email) {
        userService.resetPassword(email);
        return new MResponse("Reset email sent");
    }

    @PostMapping("/password")
    @ApiOperation(
            value = "Update user password"
    )
    public MResponse updatePassword(@RequestBody MUpdatePassword updatePassword) {
        userService.updatePassword(updatePassword);
        return new MResponse("Password updated");
    }

}
