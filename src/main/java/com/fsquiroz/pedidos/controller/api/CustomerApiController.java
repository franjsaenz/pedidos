package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MCustomer;
import com.fsquiroz.pedidos.entity.json.MResponse;
import com.fsquiroz.pedidos.security.SecurityCheck;
import com.fsquiroz.pedidos.service.declaration.ICustomerService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/customers")
public class CustomerApiController {

    private SecurityCheck securityCheck;

    private ICustomerService customerService;

    public CustomerApiController(SecurityCheck securityCheck, ICustomerService customerService) {
        this.securityCheck = securityCheck;
        this.customerService = customerService;
    }

    @GetMapping
    @ApiOperation(
            value = "List customers",
            notes = "List all customers"
    )
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                value = "Page number. Range from 0 to N")
        ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                value = "Page size. Number of elements to be retrieved. Rage from 1 to N")
        ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public Page<MCustomer> list(@ApiIgnore Pageable page, @RequestParam(required = false) String name) {
        Page<Customer> customers = customerService.list(securityCheck.getAuthenticationShop(), name, page);
        return customerService.build(customers);
    }

    @PostMapping
    @ApiOperation(
            value = "Create customer",
            notes = "Create a new customer"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MCustomer create(@RequestBody MCustomer customer) {
        User u = securityCheck.getAuthentication();
        Customer c = customerService.create(u.getShop(), customer, u);
        return customerService.build(c);
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Get customer",
            notes = "Get customer by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MCustomer get(@PathVariable String id) {
        Customer c = customerService.get(id);
        return customerService.build(c);
    }

    @PostMapping("/{id}")
    @ApiOperation(
            value = "Update customer",
            notes = "Update customer information"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MCustomer update(@PathVariable String id, @RequestBody MCustomer customer) {
        Customer c = customerService.get(id);
        c = customerService.update(c, customer, securityCheck.getAuthentication());
        return customerService.build(c);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "Delete user",
            notes = "Delete user by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MResponse delete(@PathVariable String id) {
        Customer c = customerService.get(id);
        customerService.delete(c, securityCheck.getAuthentication());
        return new MResponse("Customer deleted");
    }

}
