package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.json.MResponse;
import com.fsquiroz.pedidos.entity.json.MShop;
import com.fsquiroz.pedidos.entity.json.MStats;
import com.fsquiroz.pedidos.security.SecurityCheck;
import com.fsquiroz.pedidos.service.StatService;
import com.fsquiroz.pedidos.service.declaration.IShopService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/shops")
public class ShopApiController {

    private SecurityCheck securityCheck;

    private IShopService shopService;

    private StatService statService;

    public ShopApiController(
            SecurityCheck securityCheck,
            IShopService shopService,
            StatService statService
    ) {
        this.securityCheck = securityCheck;
        this.shopService = shopService;
        this.statService = statService;
    }

    @GetMapping
    @ApiOperation(
            value = "List shops",
            notes = "List all shops"
    )
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                value = "Page number. Range from 0 to N")
        ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                value = "Page size. Number of elements to be retrieved. Rage from 1 to N")
        ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @PreAuthorize("hasAnyRole('SUPER')")
    public Page<MShop> list(@ApiIgnore Pageable page, @RequestParam(required = false) String name) {
        Page<Shop> shops = shopService.list(name, page);
        return shopService.build(shops);
    }

    @PostMapping
    @ApiOperation(
            value = "Create shop",
            notes = "Create a new shop"
    )
    @PreAuthorize("hasAnyRole('SUPER')")
    public MShop create(@RequestBody MShop shop) {
        Shop s = shopService.create(shop, securityCheck.getAuthentication());
        return shopService.build(s);
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Get shop",
            notes = "Get shop by id"
    )
    @PreAuthorize("hasAnyRole('SUPER')")
    public MShop get(@PathVariable String id) {
        Shop s = shopService.get(id);
        return shopService.build(s);
    }

    @GetMapping("/self")
    @ApiOperation(
            value = "Get shop",
            notes = "Get loggedin shop"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MShop self() {
        Shop s = securityCheck.getAuthenticationShop();
        return shopService.build(s);
    }

    @GetMapping("/self/stats")
    @ApiOperation(
            value = "Get shop",
            notes = "Get loggedin shop"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MStats getStats() {
        Shop s = securityCheck.getAuthenticationShop();
        return statService.getStats(s);
    }

    @PostMapping("/{id}")
    @ApiOperation(
            value = "Update shop",
            notes = "Update shop information"
    )
    @PreAuthorize("hasAnyRole('SUPER')")
    public MShop updatge(@PathVariable String id, @RequestBody MShop shop) {
        Shop s = shopService.get(id);
        s = shopService.update(s, shop, securityCheck.getAuthentication());
        return shopService.build(s);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "Delete shop",
            notes = "Delete shop by id"
    )
    @PreAuthorize("hasAnyRole('SUPER')")
    public MResponse delete(@PathVariable String id) {
        Shop s = shopService.get(id);
        shopService.delete(s, securityCheck.getAuthentication());
        return new MResponse("Shop deleted");
    }

}
