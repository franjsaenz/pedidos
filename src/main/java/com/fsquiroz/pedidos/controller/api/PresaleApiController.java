package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Detail;
import com.fsquiroz.pedidos.entity.db.Presale;
import com.fsquiroz.pedidos.entity.db.Status;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MDetail;
import com.fsquiroz.pedidos.entity.json.MPresale;
import com.fsquiroz.pedidos.entity.json.MResponse;
import com.fsquiroz.pedidos.security.SecurityCheck;
import com.fsquiroz.pedidos.service.declaration.ICustomerService;
import com.fsquiroz.pedidos.service.declaration.IPresaleService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/presales")
public class PresaleApiController {

    private SecurityCheck securityCheck;

    private IPresaleService presaleService;

    private ICustomerService customerService;

    public PresaleApiController(SecurityCheck securityCheck, IPresaleService presaleService, ICustomerService customerService) {
        this.securityCheck = securityCheck;
        this.presaleService = presaleService;
        this.customerService = customerService;
    }

    @GetMapping
    @ApiOperation(
            value = "List presales",
            notes = "List all presales"
    )
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                value = "Page number. Range from 0 to N")
        ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                value = "Page size. Number of elements to be retrieved. Rage from 1 to N")
        ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public Page<MPresale> list(@ApiIgnore Pageable page, @RequestParam(required = false) List<Status> statuses,
            @RequestParam(required = false) String customerId) {
        Customer c = null;
        if (customerId != null) {
            c = customerService.get(customerId);
        }
        Page<Presale> presales = presaleService.filter(securityCheck.getAuthenticationShop(), statuses, c, page);
        return presaleService.build(presales);
    }

    @PostMapping
    @ApiOperation(
            value = "Create presale",
            notes = "Create a new presale"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MPresale create(@RequestBody MPresale presale) {
        User u = securityCheck.getAuthentication();
        Presale p = presaleService.create(u.getShop(), presale, u);
        return presaleService.build(p);
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Get presale",
            notes = "Get presale by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MPresale get(@PathVariable String id) {
        Presale p = presaleService.get(id);
        return presaleService.build(p);
    }

    @GetMapping("/friendly/{friendlyId}")
    @ApiOperation(
            value = "Get presale",
            notes = "Get presale by friendly id"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MPresale get(@PathVariable Long friendlyId) {
        Presale p = presaleService.get(securityCheck.getAuthenticationShop(), friendlyId);
        return presaleService.build(p);
    }

    @PostMapping("/{id}")
    @ApiOperation(
            value = "Update presale",
            notes = "Update presale information"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MPresale update(@PathVariable String id, @RequestBody MPresale presale) {
        Presale p = presaleService.get(id);
        p = presaleService.update(p, presale, securityCheck.getAuthentication());
        return presaleService.build(p);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "Delete presale",
            notes = "Delete presale by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MResponse delete(@PathVariable String id) {
        Presale p = presaleService.get(id);
        presaleService.delete(p, securityCheck.getAuthentication());
        return new MResponse("Presale deleted");
    }

    @GetMapping("/{presaleId}/details")
    @ApiOperation(
            value = "List presale's details",
            notes = "List all presale's details"
    )
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                value = "Page number. Range from 0 to N")
        ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                value = "Page size. Number of elements to be retrieved. Rage from 1 to N")
        ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public Page<MDetail> details(@ApiIgnore Pageable page, @PathVariable String presaleId) {
        Presale p = presaleService.get(presaleId);
        Page<Detail> details = presaleService.listDetails(securityCheck.getAuthenticationShop(), p, page);
        return presaleService.buildDetails(details);
    }

    @PostMapping("/{presaleId}/details")
    @ApiOperation(
            value = "Update presale's details",
            notes = "Update presale's details information"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MDetail add(@PathVariable String presaleId, @RequestBody MDetail detail) {
        Presale p = presaleService.get(presaleId);
        Detail d = presaleService.add(p, detail, securityCheck.getAuthentication());
        return presaleService.buildDetail(d);
    }

    @PostMapping("/{presaleId}/details/{detailId}")
    @ApiOperation(
            value = "Update presale's details by id",
            notes = "Update presale's details information by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MDetail update(@PathVariable String presaleId, @PathVariable String detailId, @RequestBody MDetail detail) {
        Presale p = presaleService.get(presaleId);
        Detail d = presaleService.getDetail(detailId);
        d = presaleService.update(p, d, detail, securityCheck.getAuthentication());
        return presaleService.buildDetail(d);
    }

    @DeleteMapping("/{presaleId}/details/{detailId}")
    @ApiOperation(
            value = "Delete presale's details",
            notes = "Delete presale's details by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MResponse remove(@PathVariable String presaleId, @PathVariable String detailId) {
        Presale p = presaleService.get(presaleId);
        Detail d = presaleService.getDetail(detailId);
        presaleService.remove(p, d, securityCheck.getAuthentication());
        return new MResponse("Detail remove");
    }

}
