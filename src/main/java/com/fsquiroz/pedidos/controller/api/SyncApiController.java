package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Product;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.json.MAsyncPresale;
import com.fsquiroz.pedidos.entity.json.MCustomer;
import com.fsquiroz.pedidos.entity.json.MProduct;
import com.fsquiroz.pedidos.entity.json.MResponse;
import com.fsquiroz.pedidos.entity.json.MSync;
import com.fsquiroz.pedidos.security.SecurityCheck;
import com.fsquiroz.pedidos.service.declaration.ICustomerService;
import com.fsquiroz.pedidos.service.declaration.IPresaleService;
import com.fsquiroz.pedidos.service.declaration.IProductService;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sync")
public class SyncApiController {

    private SecurityCheck securityCheck;

    private ICustomerService customerService;

    private IProductService productService;

    private IPresaleService presaleService;

    public SyncApiController(
            SecurityCheck securityCheck,
            ICustomerService customerService,
            IProductService productService,
            IPresaleService presaleService
    ) {
        this.securityCheck = securityCheck;
        this.customerService = customerService;
        this.productService = productService;
        this.presaleService = presaleService;
    }

    @GetMapping
    @ApiOperation(
            value = "Synchronization bundle"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MSync getSync() {
        Shop shop = securityCheck.getAuthenticationShop();
        List<Customer> customers = customerService.getSync(shop);
        List<Product> products = productService.getSync(shop);
        List<MCustomer> mcs = new ArrayList<>();
        List<MProduct> mps = new ArrayList<>();
        for (Customer c : customers) {
            mcs.add(customerService.build(c));
        }
        for (Product p : products) {
            mps.add(productService.build(p));
        }
        return new MSync(mcs, mps);
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MResponse create(@RequestBody MAsyncPresale map) {
        presaleService.createAsync(map, securityCheck.getAuthentication());
        return new MResponse("Created");
    }

}
