package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.db.Category;
import com.fsquiroz.pedidos.entity.db.Photo;
import com.fsquiroz.pedidos.entity.db.Product;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MPhoto;
import com.fsquiroz.pedidos.entity.json.MProduct;
import com.fsquiroz.pedidos.entity.json.MResponse;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.security.SecurityCheck;
import com.fsquiroz.pedidos.service.PhotoService;
import com.fsquiroz.pedidos.service.declaration.ICategoryService;
import com.fsquiroz.pedidos.service.declaration.IProductService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/products")
public class ProductApiController {

    private SecurityCheck securityCheck;

    private IProductService productService;

    private ICategoryService categoryService;

    private PhotoService photoService;

    public ProductApiController(
            SecurityCheck securityCheck,
            IProductService productService,
            ICategoryService categoryService,
            PhotoService photoService
    ) {
        this.securityCheck = securityCheck;
        this.productService = productService;
        this.categoryService = categoryService;
        this.photoService = photoService;
    }

    @GetMapping
    @ApiOperation(
            value = "List products",
            notes = "List all products"
    )
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                value = "Page number. Range from 0 to N")
        ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                value = "Page size. Number of elements to be retrieved. Rage from 1 to N")
        ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public Page<MProduct> list(@ApiIgnore Pageable page, @RequestParam(required = false) String name,
            @RequestParam(required = false) List<String> categoryId) {
        List<Category> categories = null;
        if (categoryId != null) {
            categories = new ArrayList<>();
            for (String id : categoryId) {
                categories.add(categoryService.get(id));
            }
        }
        Page<Product> products = productService.listByCategory(securityCheck.getAuthenticationShop(), name, categories, page);
        return productService.build(products);
    }

    @PostMapping
    @ApiOperation(
            value = "Create product",
            notes = "Create a new product"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MProduct create(@RequestBody MProduct product) {
        User u = securityCheck.getAuthentication();
        Product p = productService.create(u.getShop(), product, u);
        return productService.build(p);
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Get product",
            notes = "Get product by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MProduct get(@PathVariable String id) {
        Product p = productService.get(id);
        return productService.build(p);
    }

    @PostMapping("/{id}")
    @ApiOperation(
            value = "Update product",
            notes = "Update product information"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MProduct update(@PathVariable String id, @RequestBody MProduct product) {
        Product p = productService.get(id);
        p = productService.update(p, product, securityCheck.getAuthentication());
        return productService.build(p);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "Delete product",
            notes = "Delete product by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MResponse delete(@PathVariable String id) {
        Product p = productService.get(id);
        productService.delete(p, securityCheck.getAuthentication());
        return new MResponse("Product deleted");
    }

    @GetMapping("/{id}/photo")
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MPhoto getPhoto(@PathVariable String id) {
        Product p = productService.get(id);
        if (p.getPhoto() == null) {
            throw new NotFoundException(Photo.class);
        }
        return photoService.getBin(p.getPhoto());
    }

    @PostMapping("/{id}/photo")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MResponse addPhoto(@PathVariable String id, @RequestBody MPhoto photo) {
        User u = securityCheck.getAuthentication();
        Product p = productService.get(id);
        Photo ph = photoService.create(u, photo, p);
        productService.addPhoto(u, p, ph);
        return new MResponse(photoService.getUrl(ph));
    }

    @DeleteMapping("/{id}/photo")
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MResponse delPhoto(@PathVariable String id) {
        User u = securityCheck.getAuthentication();
        Product p = productService.get(id);
        productService.delPhoto(u, p);
        return new MResponse("Photo deleted");
    }

}
