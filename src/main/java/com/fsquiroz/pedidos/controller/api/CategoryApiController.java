package com.fsquiroz.pedidos.controller.api;

import com.fsquiroz.pedidos.entity.db.Category;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.entity.json.MCategory;
import com.fsquiroz.pedidos.entity.json.MResponse;
import com.fsquiroz.pedidos.security.SecurityCheck;
import com.fsquiroz.pedidos.service.declaration.ICategoryService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/categories")
public class CategoryApiController {

    private SecurityCheck securityCheck;

    private ICategoryService categoryService;

    public CategoryApiController(SecurityCheck securityCheck, ICategoryService categoryService) {
        this.securityCheck = securityCheck;
        this.categoryService = categoryService;
    }

    @GetMapping
    @ApiOperation(
            value = "List categories",
            notes = "List all categories"
    )
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                value = "Page number. Range from 0 to N")
        ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                value = "Page size. Number of elements to be retrieved. Rage from 1 to N")
        ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public Page<MCategory> list(@ApiIgnore Pageable page, @RequestParam(required = false) String name,
            @RequestParam(required = false, defaultValue = "false") Boolean withChilds) {
        Page<Category> categories = categoryService.list(securityCheck.getAuthenticationShop(), name, page);
        return categoryService.build(categories, withChilds);
    }

    @PostMapping
    @ApiOperation(
            value = "Create category",
            notes = "Create a new category"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MCategory create(@RequestBody MCategory category) {
        User u = securityCheck.getAuthentication();
        Category c = categoryService.create(u.getShop(), category, u);
        return categoryService.build(c, true);
    }

    @GetMapping("/{id}")
    @ApiOperation(
            value = "Get category",
            notes = "Get category by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public MCategory get(@PathVariable String id, @RequestParam(required = false, defaultValue = "false") Boolean withChilds) {
        Category c = categoryService.get(id);
        return categoryService.build(c, withChilds);
    }

    @GetMapping("/{id}/childs")
    @ApiOperation(
            value = "List child categories",
            notes = "List all child categories by parent"
    )
    @ApiImplicitParams({
        @ApiImplicitParam(name = "page", dataType = "int", paramType = "query",
                value = "Page number. Range from 0 to N")
        ,
            @ApiImplicitParam(name = "size", dataType = "int", paramType = "query",
                value = "Page size. Number of elements to be retrieved. Rage from 1 to N")
        ,
            @ApiImplicitParam(name = "sort", dataType = "string", paramType = "query",
                value = "Sort result. Order by property. Applicable to some first level properties. Ex: sort=name,desc")
    })
    @PreAuthorize("hasAnyRole('ADMIN', 'SELLER')")
    public Page<MCategory> listChilds(@ApiIgnore Pageable page, @RequestParam(required = false) String name,
            @PathVariable String id) {
        Category parent = categoryService.get(id);
        Page<Category> categories = categoryService.findByParent(securityCheck.getAuthenticationShop(), parent, name, page);
        return categoryService.build(categories, true);
    }

    @PostMapping("/{id}")
    @ApiOperation(
            value = "Update category",
            notes = "Update category information"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MCategory update(@PathVariable String id, @RequestBody MCategory category) {
        Category c = categoryService.get(id);
        c = categoryService.update(c, category, securityCheck.getAuthentication());
        return categoryService.build(c, false);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "Delete category",
            notes = "Delete category by id"
    )
    @PreAuthorize("hasAnyRole('ADMIN')")
    public MResponse delete(@PathVariable String id) {
        Category c = categoryService.get(id);
        categoryService.delete(c, securityCheck.getAuthentication());
        return new MResponse("Category deleted");
    }

}
