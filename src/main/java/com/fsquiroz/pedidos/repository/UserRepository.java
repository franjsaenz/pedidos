package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    public Optional<User> findOneByShopAndEmail(Shop shop, String email);

    public List<User> findByEmail(String email);

}
