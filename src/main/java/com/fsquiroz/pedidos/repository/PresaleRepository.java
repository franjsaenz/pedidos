package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Presale;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.Status;
import com.fsquiroz.pedidos.entity.db.User;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PresaleRepository extends MongoRepository<Presale, String> {

    public Optional<Presale> findOneByShopAndFriendlyNumber(Shop shop, long friendlyNumber);

    public List<Presale> findByShopAndDeletedIsNullAndStatusIn(Shop shop, Collection<Status> statuses);

    public Page<Presale> findByShopAndDeletedIsNull(Shop shop, Pageable page);

    public Page<Presale> findByShopAndDeletedIsNullAndCreator(Shop shop, User user, Pageable page);

    public Page<Presale> findByShopAndDeletedIsNullAndCustomer(Shop shop, Customer customer, Pageable page);

    public Page<Presale> findByShopAndDeletedIsNullAndStatus(Shop shop, Status status, Pageable page);

}
