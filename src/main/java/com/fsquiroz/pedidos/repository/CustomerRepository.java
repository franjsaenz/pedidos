package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Shop;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {

    public Customer findOneByShopAndCuit(Shop shop, String cuit);

    public Page<Customer> findByShopAndDeletedIsNull(Shop shop, Pageable page);

    public List<Customer> findByShopAndDeletedIsNull(Shop shop);

}
