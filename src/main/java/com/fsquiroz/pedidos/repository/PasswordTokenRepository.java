package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.PasswordToken;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordTokenRepository extends MongoRepository<PasswordToken, String> {

}
