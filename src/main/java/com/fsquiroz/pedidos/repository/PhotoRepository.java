package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Photo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhotoRepository extends MongoRepository<Photo, String> {

}
