package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Category;
import com.fsquiroz.pedidos.entity.db.Shop;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends MongoRepository<Category, String> {

    public Category findOneByCode(String code);

    public Page<Category> findByShopAndDeletedIsNull(Shop shop, Pageable page);

}
