package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Address;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends MongoRepository<Address, String> {

}
