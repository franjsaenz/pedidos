package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Shop;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopRepository extends MongoRepository<Shop, String> {

}
