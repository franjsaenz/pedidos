package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Presale;
import com.fsquiroz.pedidos.entity.db.PresaleStatus;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.Status;
import java.util.Date;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PresaleStatusRepository extends MongoRepository<PresaleStatus, String> {

    public long countByShopAndStatusAndCreatedBetween(Shop shop, Status status, Date start, Date end);

    public long countByShopAndStatusAndNextIsNullAndCreatedBetween(Shop shop, Status status, Date start, Date end);

    public PresaleStatus findFirstByPresaleOrderByCreatedDesc(Presale presale);

    public List<PresaleStatus> findByPresaleAndCreatedBetween(Presale presale, Date start, Date end);

}
