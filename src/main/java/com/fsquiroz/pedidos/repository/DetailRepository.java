package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Detail;
import com.fsquiroz.pedidos.entity.db.Presale;
import com.fsquiroz.pedidos.entity.db.Product;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailRepository extends MongoRepository<Detail, String> {

    public Page<Detail> findByShopAndDeletedIsNullAndProduct(Shop shop, Product product, Pageable page);

    public Page<Detail> findByShopAndDeletedIsNullAndPresale(Shop shop, Presale presale, Pageable page);

    public Page<Detail> findByShopAndDeletedIsNullAndCreator(Shop shop, User user, Pageable page);

    public Page<Detail> findByShopAndDeletedIsNullAndCustomer(Shop shop, Customer customer, Pageable page);

    public List<Detail> findByShopAndDeletedIsNullAndPresale(Shop shop, Presale presale);

}
