package com.fsquiroz.pedidos.repository;

import com.fsquiroz.pedidos.entity.db.Category;
import com.fsquiroz.pedidos.entity.db.Customer;
import com.fsquiroz.pedidos.entity.db.Entity;
import com.fsquiroz.pedidos.entity.db.Presale;
import com.fsquiroz.pedidos.entity.db.Product;
import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.Status;
import com.fsquiroz.pedidos.entity.db.User;
import java.util.Collection;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class SearchRepository {

    private MongoOperations opps;

    public SearchRepository(MongoOperations opps) {
        this.opps = opps;
    }

    public Page<Shop> searchShops(String name, Pageable page) {
        Query q = basicQuery();
        if (name != null && !name.isEmpty()) {
            q.addCriteria(search(name, "name"));
        }
        return buildPage(q, Shop.class, page);
    }

    public Page<User> searchUsers(Shop shop, String name, Pageable page) {
        Query q = basicQuery(shop);
        if (name != null && !name.isEmpty()) {
            q.addCriteria(new Criteria().orOperator(search(name, "lastName"), search(name, "firstName")));
        }
        return buildPage(q, User.class, page);
    }

    public Page<Customer> searchCustomer(Shop shop, String term, Pageable page) {
        Query q = basicQuery(shop);
        if (term != null && !term.isEmpty()) {
            q.addCriteria(new Criteria().orOperator(search(term, "name"), search(term, "cuit")));
        }
        return buildPage(q, Customer.class, page);
    }

    public Page<Category> searchCategories(Shop shop, String name, Category parent, boolean forceParent, Pageable pageable) {
        Query q = basicQuery(shop);
        if (name != null && !name.isEmpty()) {
            q.addCriteria(new Criteria().orOperator(search(name, "code"), search(name, "name")));
        }
        if (forceParent || parent != null) {
            q.addCriteria(Criteria.where("parent").is(parent));
        }
        return buildPage(q, Category.class, pageable);
    }

    public Page<Product> searchProducts(Shop shop, String name, Collection<Category> categories, Pageable page) {
        Query q = basicQuery(shop);
        if (name != null && !name.isEmpty()) {
            q.addCriteria(new Criteria().orOperator(search(name, "code"), search(name, "name")));
        }
        if (categories != null && !categories.isEmpty()) {
            q.addCriteria(Criteria.where("category").in(categories));
        }
        return buildPage(q, Product.class, page);
    }

    public Page<Presale> searchPresales(Shop shop, Collection<Status> statuses, Customer customer, Pageable page) {
        Query q = basicQuery(shop);
        if (statuses != null && !statuses.isEmpty()) {
            q.addCriteria(Criteria.where("status").in(statuses));
        }
        if (customer != null) {
            q.addCriteria(Criteria.where("customer").is(customer));
        }
        return buildPage(q, Presale.class, page);
    }

    private Query basicQuery() {
        Query q = new Query();
        q.addCriteria(Criteria.where("deleted").is(null));
        return q;
    }

    private Query basicQuery(Shop shop) {
        Query q = basicQuery();
        q.addCriteria(Criteria.where("shop").is(shop));
        return q;
    }

    private Criteria search(String term, String field) {
        return Criteria.where(field).regex(".*" + term + ".*", "i");
    }

    private <T extends Entity> Page<T> buildPage(Query q, Class<T> clazz, Pageable page) {
        long count = opps.count(q, clazz);
        q.with(page);
        List<T> result = opps.find(q, clazz);
        return new PageImpl<>(result, page, count);
    }

}
