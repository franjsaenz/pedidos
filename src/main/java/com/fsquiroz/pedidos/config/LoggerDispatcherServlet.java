package com.fsquiroz.pedidos.config;

import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.security.SecurityCheck;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExecutionChain;

@Component
@Slf4j
public class LoggerDispatcherServlet extends DispatcherServlet {

    @Autowired
    private SecurityCheck securityCheck;

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long start = System.currentTimeMillis();
        HandlerExecutionChain handler = getHandler(request);
        try {
            super.doDispatch(request, response);
        } finally {
            if (request.getRequestURL().toString().contains("/api/")) {
                long end = System.currentTimeMillis();
                debug(request, response, handler, end - start);
            }
        }
    }

    private void debug(HttpServletRequest request,
            HttpServletResponse response,
            HandlerExecutionChain handler,
            long elapsed) {
        User u;
        try {
            u = securityCheck.getAuthentication();
        } catch (Exception e) {
            u = null;
        }
        String user;
        String role;
        if (u != null) {
            user = u.getLastName() + ", " + u.getFirstName() + " (" + u.getEmail() + ")";
        } else {
            user = "ANONYMOUS";
        }
        if (u != null && u.getRole() != null) {
            role = u.getRole().getName();
        } else {
            role = "ANONYMOUS";
        }
        log.debug("\n"
                + "HTTP Request:\n"
                + "\turl: {}\n"
                + "\tmethod: {}\n"
                + "\tclient: {}\n"
                + "\tcredential: {}\n"
                + "\tuser: {}\n"
                + "\trole: {}\n"
                + "Handler:\n"
                + "\tmethod: {}\n"
                + "\ttook: {}ms\n"
                + "HTTP Response:\n"
                + "\tstatus: {}\n",
                request.getRequestURL(),
                request.getMethod(),
                request.getRemoteAddr(),
                u != null ? u.getId() : "ANONYMOUS",
                user,
                role,
                handler.toString(),
                elapsed,
                response.getStatus());
    }

}
