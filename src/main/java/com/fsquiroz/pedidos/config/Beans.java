package com.fsquiroz.pedidos.config;

import com.google.common.base.Predicates;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import java.util.Arrays;
import java.util.Collections;
import lombok.Data;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableConfigurationProperties(Beans.MongoConfig.class)
public class Beans {

    private Docket docket;

    private PasswordEncoder encoder;

    private ServletRegistrationBean servletRegistrationBean;

    private MongoConfig mongoConfig;

    @Autowired
    private LoggerDispatcherServlet loggerDispatcherServlet;

    @Bean
    public MongoConfig mongoConfig() {
        if (this.mongoConfig == null) {
            this.mongoConfig = new MongoConfig();
        }
        return this.mongoConfig;
    }

    @Bean
    public MongoClient mongoClient() {
        mongoConfig();
        String uri = mongoConfig.buildUri();
        return new MongoClient(new MongoClientURI(uri));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        if (encoder == null) {
            encoder = new PasswordEncoder() {
                private StrongPasswordEncryptor spe = new StrongPasswordEncryptor();

                @Override
                public String encode(CharSequence rawPassword) {
                    return spe.encryptPassword(rawPassword.toString());
                }

                @Override
                public boolean matches(CharSequence rawPassword, String encodedPassword) {
                    return spe.checkPassword(rawPassword.toString(), encodedPassword);
                }
            };
        }
        return encoder;
    }

    @Bean
    public ServletRegistrationBean dispatcherRegistration() {
        if (this.servletRegistrationBean == null) {
            this.servletRegistrationBean = new ServletRegistrationBean(loggerDispatcherServlet);
        }
        return this.servletRegistrationBean;
    }

    @Bean
    @SuppressWarnings("unchecked")
    public Docket api() {
        if (docket == null) {
            docket = new Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .paths(PathSelectors.ant("/api/**"))
                    .build()
                    .apiInfo(
                            new ApiInfo(
                                    "Pedidos",
                                    "Pedidos RESTful API Engine",
                                    "1.0",
                                    null,
                                    new Contact("fsQuiroz", "https://fsquiroz.com/", "dev@fsquiroz.com"),
                                    null,
                                    null,
                                    Collections.EMPTY_LIST
                            )
                    )
                    .securitySchemes(Arrays.asList(authorizationBearer()))
                    .securityContexts(Arrays.asList(securityContext()));
        }
        return docket;
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(
                        Arrays.asList(new SecurityReference(
                                "apiKey",
                                new AuthorizationScope[]{new AuthorizationScope("global", "Global access")}
                        ))
                )
                .forPaths(
                        Predicates.and(
                                PathSelectors.ant("/api/**"),
                                Predicates.not(PathSelectors.ant("/api/login")),
                                Predicates.not(PathSelectors.ant("/api/password"))
                        )
                ).build();
    }

    private ApiKey authorizationBearer() {
        return new ApiKey("apiKey", "Authorization", "header");
    }

    @ConfigurationProperties(prefix = "spring.data.mongodb")
    @Data
    public static class MongoConfig {

        private String host;

        private String port;

        private String database;

        private String username;

        private String password;

        public String buildUri() {
            String uri = "mongodb://";
            if (username != null && !username.isEmpty()) {
                if (password != null && !password.isEmpty()) {
                    uri += username + ":" + password + "@";
                } else {
                    uri += username + "@";
                }
            }
            uri += host + ":" + port;
            return uri;
        }

    }

}
