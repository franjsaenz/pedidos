package com.fsquiroz.pedidos.security;

import com.fsquiroz.pedidos.entity.db.Shop;
import com.fsquiroz.pedidos.entity.db.User;
import com.fsquiroz.pedidos.exception.AppErrorCode;
import com.fsquiroz.pedidos.exception.NotFoundException;
import com.fsquiroz.pedidos.exception.UnauthorizedException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityCheck {

    @Autowired
    private MongoOperations opps;

    @Value("${security.jwt.secret}")
    private String jwtSecret;

    public User get(String id) {
        User u = opps.findById(id, User.class);
        if (u == null) {
            throw new NotFoundException(User.class);
        }
        return u;
    }

    public User authenticate(String token) {
        String id = parseToken(token);
        return get(id);
    }

    public String parseToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
        return claims.get("userId", String.class);
    }

    public String generateToken(User u) {
        Claims claims = Jwts.claims().setSubject(u.getEmail());
        claims.put("userId", u.getId());
        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
        return token;
    }

    public User getAuthentication() {
        User u = null;
        SecurityContext context = SecurityContextHolder.getContext();
        if (context != null) {
            Authentication a = context.getAuthentication();
            if (a != null) {
                Object o = a.getPrincipal();
                if (o != null) {
                    u = (User) o;
                }
            }
        }
        if (u == null) {
            throw new UnauthorizedException(AppErrorCode.UNAUTHORIZED);
        }
        return u;
    }

    public Shop getAuthenticationShop() {
        return getAuthentication().getShop();
    }

}
