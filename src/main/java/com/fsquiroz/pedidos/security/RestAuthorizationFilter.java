package com.fsquiroz.pedidos.security;

import com.fsquiroz.pedidos.entity.db.User;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class RestAuthorizationFilter extends BasicAuthenticationFilter {

    @Autowired
    private SecurityCheck securityCheck;

    public RestAuthorizationFilter() {
        super(new AuthenticationManager() {
            @Override
            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader("Authorization");
        String token = null;
        if (header != null && header.startsWith("Bearer ")) {
            token = header.substring(7);
        }
        if (token == null) {
            chain.doFilter(request, response);
            return;
        }
        UsernamePasswordAuthenticationToken auth = getAuth(token, request);
        if (auth == null) {
            SecurityContextHolder.clearContext();
            chain.doFilter(request, response);
            return;
        }
        SecurityContextHolder.getContext().setAuthentication(auth);
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuth(String token, HttpServletRequest request) {
        Assert.hasText(token, "'token' must not be empty");
        try {
            User u = securityCheck.authenticate(token);
            if (u != null) {
                return new UsernamePasswordAuthenticationToken(u, null, u.getAuthorities());
            }
        } catch (Exception e) {
        }
        return null;
    }

}
