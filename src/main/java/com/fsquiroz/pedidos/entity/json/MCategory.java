package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.LinkedHashSet;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Category")
public class MCategory extends MEntity {

    @ApiModelProperty(required = false, example = "5afc9ffcd9ea183949d2d138")
    private String id;

    @ApiModelProperty(required = true, example = "AB100")
    private String code;

    @ApiModelProperty(required = true, example = "Cleaning")
    private String name;

    @ApiModelProperty(required = false, value = "Childs categories. Only on response.")
    private Set<MCategory> childs = new LinkedHashSet<>();

    @ApiModelProperty(required = false, value = "Parent category id. Only on request.")
    private String parentId;

}
