package com.fsquiroz.pedidos.entity.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Login")
public class MLogin {

    @ApiModelProperty(required = true, example = "mail@example.com")
    private String email;

    @ApiModelProperty(required = true, example = "s3cr3tP4ssw0rd")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

}
