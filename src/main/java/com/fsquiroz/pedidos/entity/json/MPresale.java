package com.fsquiroz.pedidos.entity.json;

import com.fsquiroz.pedidos.entity.db.Status;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Presale")
public class MPresale extends MEntity {

    @ApiModelProperty(required = false, example = "5afc9ffcd9ea183949d2d138")
    private String id;

    @ApiModelProperty(required = false, value = "Presale user. Only on response.")
    private MUser user;

    @ApiModelProperty(required = false, example = "2018-08-15T18:19:57.351+0000")
    private Date deliverDate;

    @ApiModelProperty(required = true, example = "15.60")
    private double shippingPrice;

    private Date lastStatusUpdate;

    @ApiModelProperty(required = false, value = "User role: [PENDING, SENT, DELIVER, CANCEL]")
    private Status status;

    @ApiModelProperty(required = false, example = "2018-08-15T18:19:57.351+0000")
    private Date delivered;

    @ApiModelProperty(required = true, example = "265")
    private long friendlyNumber;

    @ApiModelProperty(required = true, example = "20.40")
    private double subtotal;

    @ApiModelProperty(required = true, example = "36.00")
    private double total;

    @ApiModelProperty(required = false, value = "Presale address. Only on response.")
    private MAddress address;

    @ApiModelProperty(required = false, value = "Presale address id. Only on request.")
    private String addressId;

    @ApiModelProperty(required = false, value = "Presale customer. Only on response.")
    private MCustomer customer;

    @ApiModelProperty(required = false, value = "Presale customer id. Only on request.")
    private String customerId;

}
