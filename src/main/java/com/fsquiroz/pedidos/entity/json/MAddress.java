package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Address")
public class MAddress extends MEntity {

    @ApiModelProperty(required = false, example = "5afc9ffcd9ea183949d2d138")
    private String id;

    @ApiModelProperty(required = true, example = "McArthur St.")
    private String street;

    @ApiModelProperty(required = false, example = "123")
    private Integer number;

    @ApiModelProperty(required = true, example = "Upperwoods")
    private String district;

    @ApiModelProperty(required = false, example = "-32.78")
    private Double latitude;

    @ApiModelProperty(required = false, example = "-64.87")
    private Double longitude;

}
