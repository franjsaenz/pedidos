package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Stats")
public class MStats {

    private MStatsStatus byStatus;

    private List<MStatusCustomer> byCustomer;

    private List<MStatusUser> byUser;

    private MStatusMonthly monthly;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MStatsStatus {

        private MStatsStatusSingle pending;

        private MStatsStatusSingle sent;

        private MStatsStatusSingle deliver;

        private MStatsStatusSingle cancel;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MStatsStatusSingle {

        private long month;

        private long year;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MStatusCustomer implements Comparable<MStatusCustomer> {

        private MCustomer customer;

        private long total;

        @Override
        public int compareTo(MStatusCustomer o) {
            return Long.compare(total, o.total);
        }

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MStatusUser implements Comparable<MStatusUser> {

        private MUser user;

        private long total;

        @Override
        public int compareTo(MStatusUser o) {
            return Long.compare(total, o.total);
        }

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MStatusMonthly {

        private List<MStatusMonthlyData> data;

        private List<String> labels;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MStatusMonthlyData {

        private List<Long> data;

        private String label;

    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MStatusDatePair {

        private Date from;

        private Date to;

    }

}
