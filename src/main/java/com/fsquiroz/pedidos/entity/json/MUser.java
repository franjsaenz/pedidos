package com.fsquiroz.pedidos.entity.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fsquiroz.pedidos.entity.db.Role;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "User")
public class MUser extends MEntity {

    @ApiModelProperty(required = false, example = "5afc9ffcd9ea183949d2d138")
    private String id;

    @ApiModelProperty(required = true, example = "mail@example.com")
    private String email;

    @ApiModelProperty(required = true, example = "Douglas")
    private String firstName;

    @ApiModelProperty(required = true, example = "Underwood")
    private String lastName;

    @ApiModelProperty(required = false, value = "User role: [SUPER, CREATOR, GUEST]")
    private Role role;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ApiModelProperty(example = "*****", value = "User's bearer token to be used as 'Bearer *****' in authentication. Only show on login")
    private String token;

    @ApiModelProperty(required = true, example = "s3cr3tP4ssw0rd")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

}
