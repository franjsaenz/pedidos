package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Error")
public class MException {

    @ApiModelProperty(required = false, readOnly = true, value = "Error date")
    private Date timestamp;

    @ApiModelProperty(required = false, readOnly = true, value = "Response status code")
    private int status;

    @ApiModelProperty(required = false, readOnly = true, value = "Response status reason")
    private String error;

    @ApiModelProperty(required = false, readOnly = true, value = "Response status message")
    private String message;
    
    @ApiModelProperty(required = false, readOnly = true, value = "Response status error code")
    private String code;

    @ApiModelProperty(required = false, readOnly = true, value = "Request path")
    private String path;

}
