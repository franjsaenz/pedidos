package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AsyncPresale")
public class MAsyncPresale {

    private MPresale presale;
    
    private List<MDetail> details;
    
}
