package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Photo")
public class MPhoto extends MEntity {

    private String meta;

    private String name;

    private String base64;

}
