package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Detail")
public class MDetail extends MEntity {

    @ApiModelProperty(required = false, example = "5afc9ffcd9ea183949d2d138")
    private String id;

    @ApiModelProperty(required = true, example = "3")
    private int amount;

    @ApiModelProperty(required = true, example = "76.25")
    private double subtotal;

    @ApiModelProperty(required = true, example = "0.25")
    private double discount;

    @ApiModelProperty(required = false, value = "Detail product. Only on response.")
    private MProduct product;

    @ApiModelProperty(required = false, value = "Detail product id. Only on request.")
    private String productId;

}
