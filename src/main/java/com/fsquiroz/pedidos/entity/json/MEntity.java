package com.fsquiroz.pedidos.entity.json;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MEntity {

    private Date created;

    private MUser creator;

    private Date edited;

    private MUser editor;

    private Date deleted;

    private MUser deleter;

}
