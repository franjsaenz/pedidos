package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Shop")
public class MShop extends MEntity {

    private String id;

    private String name;

    private String friendlyName;

    private MUser user;

}
