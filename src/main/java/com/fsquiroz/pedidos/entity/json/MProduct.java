package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Product")
public class MProduct extends MEntity {

    @ApiModelProperty(required = false, example = "5afc9ffcd9ea183949d2d138")
    private String id;

    @ApiModelProperty(required = true, example = "Gloves")
    private String name;

    @ApiModelProperty(required = true, example = "CA101")
    private String code;

    @ApiModelProperty(required = true, example = "10.25")
    private double purchasePrice;

    @ApiModelProperty(required = true, example = "10.75")
    private double salePrice;

    @ApiModelProperty(required = true, example = "0.21")
    private double tax;

    @ApiModelProperty(required = false, value = "Product category. Only on response.")
    private MCategory category;

    @ApiModelProperty(required = false, value = "Product category id. Only on request.")
    private String categoryId;

    private String photo;

}
