package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import java.util.Date;
import java.util.List;
import lombok.Data;

@Data
@ApiModel(value = "MultiShopError")
public class MMultiShopException extends MException {

    private List<MShop> shops;

    public MMultiShopException() {
    }

    public MMultiShopException(
            Date timestamp,
            int status,
            String error,
            String message,
            String code,
            String path,
            List<MShop> shops
    ) {
        super(timestamp, status, error, message, code, path);
        this.shops = shops;
    }

}
