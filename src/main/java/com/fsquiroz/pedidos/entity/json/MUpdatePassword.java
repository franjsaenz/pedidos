package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ChangePassword")
public class MUpdatePassword {

    @ApiModelProperty(required = false, example = "0ldP4ssw0rd", value = "Required on change password")
    private String oldPassword;

    @ApiModelProperty(required = false, example = "secret-token-12345", value = "Required on reset password")
    private String resetToken;

    @ApiModelProperty(required = true, example = "n3wP4ssw0rd")
    private String newPassword;

}
