package com.fsquiroz.pedidos.entity.json;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Customer")
public class MCustomer extends MEntity {

    @ApiModelProperty(required = false, example = "5afc9ffcd9ea183949d2d138")
    private String id;

    @ApiModelProperty(required = true, example = "Soft Express")
    private String name;

    @ApiModelProperty(required = true, example = "11-22222222-3")
    private String cuit;

    @ApiModelProperty(required = true, example = "-123.05")
    private double balance;

    @ApiModelProperty(required = false, value = "Customer address. Only on response.")
    private MAddress address;

    @ApiModelProperty(required = false, value = "Customer address id. Only on request.")
    private String addressId;

}
