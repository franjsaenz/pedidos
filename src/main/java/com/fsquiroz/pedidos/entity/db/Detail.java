package com.fsquiroz.pedidos.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Setter
@Getter
@ToString(callSuper = true)
public class Detail extends Filterable {

    private int amount;

    private double subtotal;

    private double discount;

    @DBRef
    private Product product;

    @DBRef
    private Presale presale;

    @DBRef
    private Customer customer;

}
