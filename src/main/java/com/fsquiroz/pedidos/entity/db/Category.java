package com.fsquiroz.pedidos.entity.db;

import java.util.LinkedHashSet;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
@ToString(callSuper = true)
public class Category extends Filterable {

    private String code;

    private String name;

    @DBRef
    private Category parent;

    @DBRef
    private Set<Category> childs = new LinkedHashSet<>();

}
