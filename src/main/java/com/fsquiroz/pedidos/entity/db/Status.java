package com.fsquiroz.pedidos.entity.db;

public enum Status {

    PENDING("PENDING"),
    SENT("SENT"),
    DELIVER("DELIVER"),
    CANCEL("CANCEL");

    private String name;

    Status(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
