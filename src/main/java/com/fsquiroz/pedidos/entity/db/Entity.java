package com.fsquiroz.pedidos.entity.db;

import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

@Data
@EqualsAndHashCode(of = "id")
public abstract class Entity implements Comparable<Entity> {

    @Id
    protected String id;

    protected Date created;

    @DBRef
    protected User creator;

    protected Date edited;

    @DBRef
    protected User editor;

    protected Date deleted;

    @DBRef
    protected User deleter;

    @Override
    public int compareTo(Entity o) {
        if (o == null || o.id == null) {
            return Integer.MAX_VALUE;
        } else if (id == null) {
            return Integer.MIN_VALUE;
        } else {
            return id.compareTo(o.id);
        }
    }

}
