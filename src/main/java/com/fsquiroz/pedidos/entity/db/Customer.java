package com.fsquiroz.pedidos.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
@ToString(callSuper = true)
public class Customer extends Filterable {

    private String name;

    private String cuit;

    private double balance;

    @DBRef
    private Address address;

}
