package com.fsquiroz.pedidos.entity.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Document
@Getter
@Setter
@ToString(callSuper = true)
public class User extends Filterable implements UserDetails {

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private Role role;
    
    @Transient
    private String token;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        if (role != null) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        }
        return authorities;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return super.deleted == null;
    }

    @Override
    public boolean isAccountNonLocked() {
        return super.deleted == null;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return super.deleted == null;
    }

    @Override
    public boolean isEnabled() {
        return super.deleted == null;
    }

}
