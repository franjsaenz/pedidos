package com.fsquiroz.pedidos.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
@ToString(callSuper = true)
public class Product extends Filterable {

    private String name;

    private String code;

    private double purchasePrice;

    private double salePrice;

    private double tax;

    @DBRef
    private Category category;

    @DBRef
    private Photo photo;

}
