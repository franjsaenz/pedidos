package com.fsquiroz.pedidos.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;

@Setter
@Getter
@ToString(callSuper = true)
public abstract class Filterable extends Entity {

    @DBRef
    protected Shop shop;

}
