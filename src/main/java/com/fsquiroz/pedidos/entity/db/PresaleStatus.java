package com.fsquiroz.pedidos.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
@ToString(callSuper = true)
public class PresaleStatus extends Entity {

    @DBRef
    private Shop shop;

    @DBRef
    private Presale presale;

    private Status status;

    @DBRef
    private PresaleStatus next;

}
