package com.fsquiroz.pedidos.entity.db;

public enum Role {

    SUPER("SUPER"),
    ADMIN("ADMIN"),
    SELLER("SELLER");

    private String name;

    Role(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
