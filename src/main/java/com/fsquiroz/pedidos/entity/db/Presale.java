package com.fsquiroz.pedidos.entity.db;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
@ToString(callSuper = true)
public class Presale extends Filterable {

    private Date deliverDate;

    private double shippingPrice;

    private Status status;

    private Date delivered;

    @DBRef
    private Address deliveryAddress;

    @DBRef
    private Customer customer;

    private long friendlyNumber;

    private double subtotal;

    private double total;

}
