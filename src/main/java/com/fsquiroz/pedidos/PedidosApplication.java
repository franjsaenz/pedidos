package com.fsquiroz.pedidos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@PropertySources({
    @PropertySource(value = "classpath:application.properties")
    ,
    @PropertySource(value = "file:${user.home}${file.separator}.pedidos${file.separator}application.properties", ignoreResourceNotFound = false)
})
public class PedidosApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(PedidosApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PedidosApplication.class);
    }

}
