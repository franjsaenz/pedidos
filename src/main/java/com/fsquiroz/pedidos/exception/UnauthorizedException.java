package com.fsquiroz.pedidos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends AppException {

    public UnauthorizedException(AppErrorCode code) {
        super(code);
    }

    public UnauthorizedException(AppErrorCode code, String message) {
        super(code, message);
    }

    public UnauthorizedException(AppErrorCode code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public UnauthorizedException(AppErrorCode code, Throwable cause) {
        super(code, cause);
    }

}
