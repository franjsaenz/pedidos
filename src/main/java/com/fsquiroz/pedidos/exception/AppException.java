package com.fsquiroz.pedidos.exception;

public abstract class AppException extends RuntimeException {

    private final AppErrorCode code;

    public AppException(AppErrorCode code) {
        this.code = code;
    }

    public AppException(AppErrorCode code, String message) {
        super(message);
        this.code = code;
    }

    public AppException(AppErrorCode code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public AppException(AppErrorCode code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    public AppErrorCode getCode() {
        return code;
    }

}
