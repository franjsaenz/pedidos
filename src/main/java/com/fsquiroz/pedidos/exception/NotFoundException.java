package com.fsquiroz.pedidos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends AppException {

    public NotFoundException(AppErrorCode code) {
        super(code);
    }

    public NotFoundException(AppErrorCode code, String message) {
        super(code, message);
    }

    public NotFoundException(AppErrorCode code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public NotFoundException(AppErrorCode code, Throwable cause) {
        super(code, cause);
    }

    public NotFoundException(Class c) {
        this(AppErrorCode.ENTITY_NOT_FOUND, c.getSimpleName() + " not found");
    }

}
