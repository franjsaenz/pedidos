package com.fsquiroz.pedidos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends AppException {
    
    public BadRequestException(AppErrorCode code) {
        super(code);
    }

    public BadRequestException(AppErrorCode code, String message) {
        super(code, message);
    }

    public BadRequestException(AppErrorCode code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public BadRequestException(AppErrorCode code, Throwable cause) {
        super(code, cause);
    }

}
