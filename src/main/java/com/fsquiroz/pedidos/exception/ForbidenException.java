package com.fsquiroz.pedidos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class ForbidenException extends AppException {

    public ForbidenException(AppErrorCode code) {
        super(code);
    }

    public ForbidenException(AppErrorCode code, String message) {
        super(code, message);
    }

    public ForbidenException(AppErrorCode code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public ForbidenException(AppErrorCode code, Throwable cause) {
        super(code, cause);
    }

}
