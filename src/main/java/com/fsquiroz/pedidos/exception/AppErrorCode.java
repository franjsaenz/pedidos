package com.fsquiroz.pedidos.exception;

public enum AppErrorCode {

    MISSING_FIELD("MISSING_FIELD"),
    NOT_EMPTY_CATEGORY("NOT_EMPTY_CATEGORY"),
    MISSMATCH_PRESALE("MISSMATCH_PRESALE"),
    SENT_PRESALE("SENT_PRESALE"),
    INVALID_PRESALE_STATUS("INVALID_PRESALE_STATUS"),
    UNAUTHORIZED("UNAUTHORIZED"),
    ALREADY_USED_TOKEN("ALREADY_USED_TOKEN"),
    ALREADY_USED_EMAIL("ALREADY_USED_EMAIL"),
    RESERVED_ROLE("RESERVED_ROLE"),
    INVALID_EMAIL_PASSWORD("INVALID_EMAIL_PASSWORD"),
    MULTI_SHOP_USER("MULTI_SHOP_USER"),
    MISSMATCH_OLD_PASSWORD("MISSMATCH_OLD_PASSWORD"),
    ACCESS_DENIED("ACCESS_DENIED"),
    ENTITY_NOT_FOUND("ENTITY_NOT_FOUND"),
    INTERNAL_SERVER_ERROR("INTERNAL_SERVER_ERROR");

    private String nombre;

    AppErrorCode(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
