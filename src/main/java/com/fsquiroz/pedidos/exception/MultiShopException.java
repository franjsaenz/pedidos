package com.fsquiroz.pedidos.exception;

import com.fsquiroz.pedidos.entity.db.Shop;
import java.util.List;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
@Getter
public class MultiShopException extends AppException {

    private List<Shop> shops;

    public MultiShopException(List<Shop> shops) {
        super(AppErrorCode.MULTI_SHOP_USER);
        this.shops = shops;
    }

}
