export class RUser {
    id: string;
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    role: string;
    token: string;
}
