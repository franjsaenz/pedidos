export class RCategory {
    id: string;
    code: string;
    name: string;
    childs: Array<RCategory>;
    parentId: string;
}
