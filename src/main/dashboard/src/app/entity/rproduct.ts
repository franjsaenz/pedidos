import { RCategory } from "./rcategory";

export class RProduct {
    id: string;
    code: string;
    name: string;
    purchasePrice: number;
    salePrice: number;
    tax: number;
    categoryId: string;
    category: RCategory;
    photo?: string;
}
