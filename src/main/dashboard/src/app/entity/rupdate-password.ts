export class RUpdatePassword {
    oldPassword: string;
    resetToken: string;
    newPassword: string;
}
