import { RUser } from "./ruser";
import { RAddress } from "./raddress";
import { RCustomer } from "./rcustomer";

export class RPresale {
    id: string;
    created: string;
    user: RUser;
    deliverDate: string;
    shippingPrice: number;
    status: string;
    delivered: string;
    friendlyNumber: number;
    subtotal: number;
    total: number;
    address: RAddress;
    addressId: string;
    customer: RCustomer;
    customerId: string;
}
