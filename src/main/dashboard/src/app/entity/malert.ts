export class MAlert {
    type: string;
    message: string;
    dismissible: boolean;
}
