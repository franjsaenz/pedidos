import { RProduct } from "./rproduct";

export class RPresaleDetail {
    id: string;
    productId: string;
    amount: number;
    discount: number;
    subtotal: number;
    product: RProduct;
}
