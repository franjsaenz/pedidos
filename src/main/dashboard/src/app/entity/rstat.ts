import { RCustomer } from "./rcustomer";
import { RUser } from "./ruser";

export class RStat {
    byStatus: RStatStatus;
    byCustomer: Array<RStatusCustomer>;
    byUser: Array<RStatusUser>;
    monthly: RStatusMonthly;
}

export class RStatStatus {
    pending: RStatStatusSingle;
    sent: RStatStatusSingle;
    deliver: RStatStatusSingle;
    cancel: RStatStatusSingle;
}

export class RStatStatusSingle {
    month: number;
    year: number;
}

export class RStatusCustomer {
    customer: RCustomer;
    total: number;
}

export class RStatusUser {
    user: RUser;
    total: number;
}

export class RStatusMonthly {
    data: Array<RStatusMonthlyData>;
    labels: Array<string>;
}

export class RStatusMonthlyData {
    data: Array<number>;
    label: string;
}