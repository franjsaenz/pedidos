import { RAddress } from "./raddress";

export class RCustomer {
    id: string;
    name: string;
    cuit: string;
    balance: number;
    addressId: string;
    address: RAddress;
}
