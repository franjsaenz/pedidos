export class RAddress {
    id: string;
    street: string;
    number: number;
    district: string;
    latitude: number;
    longitude: number;
}
