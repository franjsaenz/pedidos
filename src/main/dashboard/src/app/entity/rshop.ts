import { RUser } from "./ruser";

export class RShop {
    id: string;
    created: string;
    name: string;
    friendlyName: string;
    user?: RUser;
}
