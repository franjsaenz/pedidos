import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { HttpAuthInterceptor } from '../utils/http-auth-interceptor';
import { LoginComponent } from './component/login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { HeaderComponent } from './component/header/header.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { BlankPageComponent } from './component/blank-page/blank-page.component';
import { UsersComponent } from './component/users/users.component';
import { CustomersComponent } from './component/customers/customers.component';
import { CategoriesComponent } from './component/categories/categories.component';
import { ProductsComponent } from './component/products/products.component';
import { PresalesComponent } from './component/presales/presales.component';
import { PresaleDetailComponent } from './component/presale-detail/presale-detail.component';
import { PresaleDetailDetailsComponent } from './component/presale-detail-details/presale-detail-details.component';
import { ModalProfileComponent } from './component/modal-profile/modal-profile.component';
import { NotFoundComponent } from './component/not-found/not-found.component';
import { ResetPasswordComponent } from './component/reset-password/reset-password.component';
import { UpdatePasswordComponent } from './component/update-password/update-password.component';
import { StatusPipe } from './pipe/status.pipe';
import { ShopComponent } from './component/shop/shop.component';
import { StatsComponent } from './component/stats/stats.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    SidebarComponent,
    BlankPageComponent,
    UsersComponent,
    CustomersComponent,
    CategoriesComponent,
    ProductsComponent,
    PresalesComponent,
    PresaleDetailComponent,
    PresaleDetailDetailsComponent,
    ModalProfileComponent,
    NotFoundComponent,
    ResetPasswordComponent,
    UpdatePasswordComponent,
    StatusPipe,
    ShopComponent,
    StatsComponent
  ],
  entryComponents: [
    ModalProfileComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NguiAutoCompleteModule,
    AppRoutingModule,
    ChartsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
