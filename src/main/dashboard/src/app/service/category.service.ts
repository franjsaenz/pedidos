import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { RCategory } from '../entity/rcategory';
import { RPage } from '../entity/rpage';
import { RResponse } from '../entity/rresponse';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private server: string = environment.baseUrl + '/api/categories';
  private pageSize: number = environment.pageSize;

  constructor(private http: HttpClient) { }

  public list(pageNumber: number, name: string, sort: string, desc: boolean, withChilds: boolean): Observable<RPage<RCategory>> {
    let p = { 'page': pageNumber + '', 'size': this.pageSize + '' };
    if (!(name == null || name == '')) {
      p['name'] = name;
    }
    if (!(sort == null || sort == '')) {
      let by = sort;
      if (desc) {
        by += ',desc';
      } else {
        by += ',asc';
      }
      p['sort'] = by;
    }
    if (withChilds) {
      p['withChilds'] = 'true';
    }
    const options = {
      params: p
    };
    return this.http.get<RPage<RCategory>>(this.server, options);
  }

  public create(category: RCategory): Observable<RCategory> {
    return this.http.post<RCategory>(this.server, category);
  }

  public get(id: string, withChilds: boolean): Observable<RCategory> {
    let p = {};
    if (withChilds) {
      p['withChilds'] = 'true';
    }
    const options = {
      params: p
    };
    return this.http.get<RCategory>(this.server + '/' + id, options);
  }

  public getChilds(parentId: string, pageNumber: number, name: string, sort: string, desc: boolean): Observable<RPage<RCategory>> {
    let p = { 'page': pageNumber + '', 'size': this.pageSize + '' };
    if (!(name == null || name == '')) {
      p['name'] = name;
    }
    if (!(sort == null || sort == '')) {
      let by = sort;
      if (desc) {
        by += ',desc';
      } else {
        by += ',asc';
      }
      p['sort'] = by;
    }
    const options = {
      params: p
    };
    return this.http.get<RPage<RCategory>>(this.server + '/' + parentId + '/childs', options);
  }

  public update(id: string, category: RCategory): Observable<RCategory> {
    return this.http.post<RCategory>(this.server + '/' + id, category);
  }

  public delete(id: string): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/' + id);
  }

}
