import { Injectable } from '@angular/core';
import { RUser } from '../entity/ruser';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor() { }

  public login(user: RUser) {
    this.updateLogedIn(user);
    localStorage.setItem('token', user.token);
  }

  public getLogedIn(): RUser {
    if (this.isLogin()) {
      return {
        id: localStorage.getItem('userId'),
        email: localStorage.getItem('email'),
        lastName: localStorage.getItem('lastName'),
        firstName: localStorage.getItem('firstName'),
        role: localStorage.getItem('role'),
        token: localStorage.getItem('token'),
        password: null
      };
    } else {
      return null;
    }
  }

  public updateLogedIn(user: RUser) {
    localStorage.setItem('userId', user.id);
    localStorage.setItem('email', user.email);
    localStorage.setItem('firstName', user.firstName);
    localStorage.setItem('lastName', user.lastName);
    localStorage.setItem('role', user.role);
  }

  public isLogin(): boolean {
    return localStorage.getItem('token') != null;
  }

  public logout() {
    localStorage.removeItem('userId');
    localStorage.removeItem('email');
    localStorage.removeItem('firstName');
    localStorage.removeItem('lastName');
    localStorage.removeItem('role');
    localStorage.removeItem('token');
  }

  public canActivate(url: string) {
    const user: RUser = this.getLogedIn();
    if (user == null) {
      return false;
    } else if (url == '/shops') {
      return user.role == 'SUPER';
    } else if (user.role == 'ADMIN') {
      return url != '/shops';
    } else if (user.role == 'SELLER') {
      return url.indexOf('/presales') >= 0;
    }
  }

}
