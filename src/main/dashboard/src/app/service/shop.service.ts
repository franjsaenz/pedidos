import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { RShop } from '../entity/rshop';
import { RPage } from '../entity/rpage';
import { RResponse } from '../entity/rresponse';
import { RStat } from '../entity/rstat';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  private server: string = environment.baseUrl + '/api/shops';
  private pageSize: number = environment.pageSize;

  constructor(private http: HttpClient) { }

  public list(pageNumber: number, name: string, sort: string, desc: boolean): Observable<RPage<RShop>> {
    let p = { 'page': pageNumber + '', 'size': this.pageSize + '' };
    if (!(name == null || name == '')) {
      p['name'] = name;
    }
    if (!(sort == null || sort == '')) {
      let by = sort;
      if (desc) {
        by += ',desc';
      } else {
        by += ',asc';
      }
      p['sort'] = by;
    }
    const options = {
      params: p
    };
    return this.http.get<RPage<RShop>>(this.server, options);
  }

  public create(shop: RShop): Observable<RShop> {
    return this.http.post<RShop>(this.server, shop);
  }

  public get(id: string): Observable<RShop> {
    return this.http.get<RShop>(this.server + '/' + id);
  }

  public getSelf(): Observable<RShop> {
    return this.http.get<RShop>(this.server + '/self');
  }

  public getStats(): Observable<RStat> {
    return this.http.get<RStat>(this.server + '/self/stats');
  }

  public edit(id: string, shop: RShop): Observable<RShop> {
    return this.http.post<RShop>(this.server + '/' + id, shop);
  }

  public delete(id: string): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/' + id);
  }

}
