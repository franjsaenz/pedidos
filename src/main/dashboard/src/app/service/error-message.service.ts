import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorMessageService {

  constructor() { }

  parseError(error: any): string {
    if (error == null || error.code == null) {
      return 'Ha habido un error inesperado...'
    }
    let code: string = error.code;
    switch (code) {
      case 'MISSING_FIELD':
        return 'Faltó un campo por completar. Verifique la información completada';
      case 'NOT_EMPTY_CATEGORY':
        return 'No se puede eliminar un rubro que tiene hijos.';
      case 'MISSMATCH_PRESALE':
        return 'El detalle a editar no peretenece a la preventa. Si el error persiste recargue la página.';
      case 'SENT_PRESALE':
      case 'INVALID_PRESALE_STATUS':
      return 'Este pedido no puede modificarse debido a que ya ha sido enviado al cliente.';
      case 'UNAUTHORIZED':
        return 'No posee autorización para acceder a este contenido.';
      case 'ALREADY_USED_TOKEN':
        return 'El código de password ya ha sido utilizado. Intente obteniendo uno nuevo.';
      case 'ALREADY_USED_EMAIL':
        return 'La dirección de email ingresada ya se encuentra registrada para otro usuario';
      case 'INVALID_EMAIL_PASSWORD':
        return 'Email o contraseña inválidas';
      case 'MISSMATCH_OLD_PASSWORD':
        return 'Anterior password ingresado incorrecto';
      case 'ACCESS_DENIED':
        return 'No posee permisos para acceder a este contenido';
      case 'ENTITY_NOT_FOUND':
        return 'No se puede encontrar el contenido al que está tratando de acceder';
      case 'INTERNAL_SERVER_ERROR':
      default:
        return 'Ha habido un error inesperado...';
    }
  }

}
