import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { RProduct } from '../entity/rproduct';
import { RPage } from '../entity/rpage';
import { RResponse } from '../entity/rresponse';
import { RCategory } from '../entity/rcategory';
import { RPhoto } from '../entity/rphoto';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private server: string = environment.baseUrl + '/api/products';
  private pageSize: number = environment.pageSize;

  constructor(private http: HttpClient) { }

  public list(pageNumber: number, name: string, sort: string, desc: boolean, categories: Array<RCategory>): Observable<RPage<RProduct>> {
    let params = '?page=' + pageNumber + '&size=' + this.pageSize;
    if (!(name == null || name == '')) {
      params += '&name=' + name;
    }
    if (!(sort == null || sort == '')) {
      let by = sort;
      if (desc) {
        by += ',desc';
      } else {
        by += ',asc';
      }
      params += '&sort=' + by;
    }
    if (categories != null && categories.length > 0) {
      for (let c of categories) {
        if (c.id != null) {
          params += '&categoryId=' + c.id;
        }
      }
    }
    return this.http.get<RPage<RProduct>>(this.server + params);
  }

  public create(product: RProduct): Observable<RProduct> {
    return this.http.post<RProduct>(this.server, product);
  }

  public get(id: string): Observable<RProduct> {
    return this.http.get<RProduct>(this.server + '/' + id);
  }

  public update(id: string, product: RProduct): Observable<RProduct> {
    return this.http.post<RProduct>(this.server + '/' + id, product);
  }

  public delete(id: string): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/' + id);
  }

  public addPhoto(id: string, photo: RPhoto): Observable<RResponse> {
    return this.http.post<RResponse>(this.server + '/' + id + '/photo', photo);
  }

  public delPhoto(id: string): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/' + id + '/photo');
  }

}
