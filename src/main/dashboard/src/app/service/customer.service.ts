import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { RCustomer } from '../entity/rcustomer';
import { RPage } from '../entity/rpage';
import { RResponse } from '../entity/rresponse';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private server: string = environment.baseUrl + '/api/customers';
  private pageSize: number = environment.pageSize;

  constructor(private http: HttpClient) { }

  public list(pageNumber: number, name: string, sort: string, desc: boolean): Observable<RPage<RCustomer>> {
    let p = { 'page': pageNumber + '', 'size': this.pageSize + '' };
    if (!(name == null || name == '')) {
      p['name'] = name;
    }
    if (!(sort == null || sort == '')) {
      let by = sort;
      if (desc) {
        by += ',desc';
      } else {
        by += ',asc';
      }
      p['sort'] = by;
    }
    const options = {
      params: p
    };
    return this.http.get<RPage<RCustomer>>(this.server, options);
  }

  public create(customer: RCustomer): Observable<RCustomer> {
    return this.http.post<RCustomer>(this.server, customer);
  }

  public get(id: string): Observable<RCustomer> {
    return this.http.get<RCustomer>(this.server + '/' + id);
  }

  public edit(id: string, customer: RCustomer): Observable<RCustomer> {
    return this.http.post<RCustomer>(this.server + '/' + id, customer);
  }

  public delete(id: string): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/' + id);
  }

}
