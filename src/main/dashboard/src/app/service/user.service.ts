import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { RUser } from '../entity/ruser';
import { RPage } from '../entity/rpage';
import { RResponse } from '../entity/rresponse';
import { RUpdatePassword } from '../entity/rupdate-password';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private server: string = environment.baseUrl + '/api/users';
  private pageSize: number = environment.pageSize;

  constructor(private http: HttpClient) { }

  public list(pageNumber: number, name: string, sort: string, desc: boolean): Observable<RPage<RUser>> {
    let p = { 'page': pageNumber + '', 'size': this.pageSize + '' };
    if (!(name == null || name == '')) {
      p['name'] = name;
    }
    if (!(sort == null || sort == '')) {
      let by = sort;
      if (desc) {
        by += ',desc';
      } else {
        by += ',asc';
      }
      p['sort'] = by;
    }
    const options = {
      params: p
    };
    return this.http.get<RPage<RUser>>(this.server, options);
  }

  public create(user: RUser): Observable<RUser> {
    return this.http.post<RUser>(this.server, user);
  }

  public get(id: string): Observable<RUser> {
    return this.http.get<RUser>(this.server + '/' + id);
  }

  public edit(id: string, user: RUser): Observable<RUser> {
    return this.http.post<RUser>(this.server + '/' + id, user);
  }

  public delete(id: string): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/' + id);
  }

  public updatePassword(pwd: RUpdatePassword): Observable<RResponse> {
    return this.http.post<RResponse>(this.server + '/password', pwd);
  }

}
