import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { RAddress } from '../entity/raddress';
import { RResponse } from '../entity/rresponse';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  private server: string = environment.baseUrl + '/api/addresses';

  constructor(private http: HttpClient) { }

  public create(address: RAddress): Observable<RAddress> {
    return this.http.post<RAddress>(this.server, address);
  }

  public get(id: string): Observable<RAddress> {
    return this.http.get<RAddress>(this.server + '/' + id);
  }

  public update(id: string, address: RAddress): Observable<RAddress> {
    return this.http.post<RAddress>(this.server + '/' + id, address);
  }

  public delete(id: string): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/' + id);
  }

}
