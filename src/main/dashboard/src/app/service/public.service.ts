import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment'
import { PLogin } from '../entity/plogin';
import { Observable } from 'rxjs';
import { RUser } from '../entity/ruser';
import { RResponse } from '../entity/rresponse';
import { RUpdatePassword } from '../entity/rupdate-password';
import { RShop } from '../entity/rshop';

@Injectable({
  providedIn: 'root'
})
export class PublicService {

  private server: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  login(login: PLogin, shop?: RShop): Observable<RUser> {
    let p = {};
    if (shop != null) {
      p['shopId'] = shop.id;
    }
    return this.http.post<RUser>(this.server + '/api/login', login, {params: p});
  }

  resetPassword(email: string): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/api/password', { params: { email: email } });
  }

  updatePassword(pwd: RUpdatePassword): Observable<RResponse> {
    return this.http.post<RResponse>(this.server + '/api/password', pwd);
  }

}
