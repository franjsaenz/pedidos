import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { RPresale } from '../entity/rpresale';
import { RPage } from '../entity/rpage';
import { RResponse } from '../entity/rresponse';
import { RPresaleDetail } from '../entity/rpresale-detail';
import { RCustomer } from '../entity/rcustomer';

@Injectable({
  providedIn: 'root'
})
export class PresaleService {

  private server: string = environment.baseUrl + '/api/presales';
  private pageSize: number = environment.pageSize;

  constructor(private http: HttpClient) { }

  public list(pageNumber: number, sort: string, desc: boolean, customer: RCustomer, statuses: Array<string>): Observable<RPage<RPresale>> {
    let params = '?page=' + pageNumber + '&size=' + this.pageSize;
    if (!(sort == null || sort == '')) {
      let by = sort;
      if (desc) {
        by += ',desc';
      } else {
        by += ',asc';
      }
      params += '&sort=' + by;
    }
    if (statuses != null && statuses.length > 0) {
      for (let s of statuses) {
        if (s != null) {
          params += '&statuses=' + s;
        }
      }
    }
    if (customer != null && customer.id != null) {
      params += '&customerId=' + customer.id;
    }
    return this.http.get<RPage<RPresale>>(this.server + params);
  }

  public create(presale: RPresale): Observable<RPresale> {
    return this.http.post<RPresale>(this.server, presale);
  }

  public get(id: string): Observable<RPresale> {
    return this.http.get<RPresale>(this.server + '/' + id);
  }

  public getFriendly(friendly: number): Observable<RPresale> {
    return this.http.get<RPresale>(this.server + '/friendly/' + friendly);
  }

  public update(id: string, presale: RPresale): Observable<RPresale> {
    return this.http.post<RPresale>(this.server + '/' + id, presale);
  }

  public delete(id: string): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/' + id);
  }

  public listDetails(presale: RPresale, pageNumber: number, sort: string, desc: boolean): Observable<RPage<RPresaleDetail>> {
    let params = '?page=' + pageNumber + '&size=' + this.pageSize;
    if (!(sort == null || sort == '')) {
      let by = sort;
      if (desc) {
        by += ',desc';
      } else {
        by += ',asc';
      }
      params += '&sort=' + by;
    }
    return this.http.get<RPage<RPresaleDetail>>(this.server + '/' + presale.id + '/details' + params);
  }

  public createDetail(presale: RPresale, detail: RPresaleDetail): Observable<RPresaleDetail> {
    return this.http.post<RPresaleDetail>(this.server + '/' + presale.id + '/details', detail);
  }

  public updateDetail(presale: RPresale, detail: RPresaleDetail): Observable<RPresaleDetail> {
    return this.http.post<RPresaleDetail>(this.server + '/' + presale.id + '/details/' + detail.id, detail);
  }

  public deleteDetail(presale: RPresale, detail: RPresaleDetail): Observable<RResponse> {
    return this.http.delete<RResponse>(this.server + '/' + presale.id + '/details/' + detail.id);
  }

}
