import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    switch (value) {
      case 'CANCEL':
        return 'Cancelado';
      case 'DELIVER':
        return 'Entregado';
      case 'SENT':
        return 'Enviado';
      case 'PENDING':
      default:
        return 'Pendiente';
    }
  }

}
