import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { BlankPageComponent } from './component/blank-page/blank-page.component';
import { UsersComponent } from './component/users/users.component';
import { CustomersComponent } from './component/customers/customers.component';
import { CategoriesComponent } from './component/categories/categories.component';
import { ProductsComponent } from './component/products/products.component';
import { PresalesComponent } from './component/presales/presales.component';
import { PresaleDetailComponent } from './component/presale-detail/presale-detail.component';
import { AuthGuard } from './service/auth.guard';
import { NotFoundComponent } from './component/not-found/not-found.component';
import { ResetPasswordComponent } from './component/reset-password/reset-password.component';
import { UpdatePasswordComponent } from './component/update-password/update-password.component';
import { ShopComponent } from './component/shop/shop.component';
import { StatsComponent } from './component/stats/stats.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'password', component: ResetPasswordComponent },
  { path: 'password/reset/:id', component: UpdatePasswordComponent },
  { path: 'stats', component: StatsComponent, canActivate: [AuthGuard] },
  { path: 'blank', component: BlankPageComponent, canActivate: [AuthGuard] },
  { path: 'shops', component: ShopComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'customers', component: CustomersComponent, canActivate: [AuthGuard] },
  { path: 'categories', component: CategoriesComponent, canActivate: [AuthGuard] },
  { path: 'products', component: ProductsComponent, canActivate: [AuthGuard] },
  { path: 'presales', component: PresalesComponent, canActivate: [AuthGuard] },
  { path: 'presales/:id', component: PresaleDetailComponent, canActivate: [AuthGuard] },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
