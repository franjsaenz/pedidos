import { Component, OnInit } from '@angular/core';
import { RPresale } from '../../entity/rpresale';
import { PresaleService } from '../../service/presale.service';
import { ErrorMessageService } from '../../service/error-message.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MAlert } from '../../entity/malert';
import { RCustomer } from '../../entity/rcustomer';
import { CustomerService } from '../../service/customer.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-presales',
  templateUrl: './presales.component.html',
  styleUrls: ['../layout.scss']
})
export class PresalesComponent implements OnInit {

  collapedSideBar: boolean;

  loadingAlert: MAlert = { type: 'primary', message: 'Cargando los pedidos...', dismissible: false };

  loadingPresales: boolean = false;

  alerts: Array<MAlert> = [];

  pageNumber: number = 1;

  pageSize: number = environment.pageSize;

  totalPages: number = 0;

  totalElements: number = 0;

  presales: Array<RPresale> = [];

  presaleForm: FormGroup;

  modal: ModalPresale = <ModalPresale>{};

  filter: Filter;

  constructor(
    private presaleService: PresaleService,
    private customerService: CustomerService,
    private errorMessageService: ErrorMessageService,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit() {
    this.filter = {
      customer: null,
      tmpCustomer: null,
      statuses: []
    }
    this.listPresales();
  }

  selectFilterCustomer(customer: RCustomer) {
    if (customer == null || customer.id == null) {
      this.filter.customer = null;
      this.filter.tmpCustomer = null;
    } else {
      this.filter.customer = customer;
      this.filter.tmpCustomer = customer;
    }
    this.pageNumber = 1;
    this.listPresales();
  }

  switchFilterStatus(status: string) {
    let i = this.filter.statuses.indexOf(status);
    if (i < 0) {
      this.filter.statuses.push(status);
    } else {
      this.filter.statuses.splice(i, 1);
    }
    this.listPresales();
  }

  filterIsOn(status: string) {
    let i = this.filter.statuses.indexOf(status);
    return i >= 0;
  }

  private startForm() {
    this.presaleForm = new FormGroup({
      'shippingPrice': new FormControl(this.modal.presale.shippingPrice, [
        Validators.required,
        Validators.pattern('^[0-9]+(\.[0-9]*)?$'),
        Validators.min(0)
      ]),
      'customerId': new FormControl(this.modal.presale.customerId, [
        Validators.required
      ]),
      'customerSelectedId': new FormControl()
    });
  }

  public filterObservableCustomers = (text: string) => {
    return this.customerService.list(0, text, 'name', false);
  }

  private listPresales() {
    this.loadingPresales = true;
    this.alerts.push(this.loadingAlert);
    let page = this.pageNumber - 1;
    this.presaleService.list(page, 'created', true, this.filter.customer, this.filter.statuses).subscribe(res => {
      this.loadingPresales = false;
      this.closeAlert(this.loadingAlert);
      this.presales = res.content;
      this.totalPages = res.totalPages;
      this.totalElements = res.totalElements;
    }, err => {
      this.loadingPresales = false;
      this.closeAlert(this.loadingAlert);
      const msg = this.errorMessageService.parseError(err['error']);
      this.alerts.push({ type: 'warning', message: msg, dismissible: true });
    })
  }

  save(presale: RPresale) {
    const np: RPresale = <RPresale>{
      status: 'PENDING',
      customerId: this.presaleForm.get('customerId').value,
      shippingPrice: this.presaleForm.get('shippingPrice').value
    }
    this.presaleService.create(np).subscribe(this.modalSuccess, this.modalError);
  }

  openNewModal(content) {
    this.createModal(<RPresale>{ status: 'PENDING' });
    this.openModal(content);
  }

  openModal(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeModal();
    }, (reason) => {
      this.closeModal();
    });
  }

  private closeModal() {
    this.createModal(<RPresale>{});
  }

  private createModal(presale: RPresale) {
    this.modal = {
      presale: presale,
      successMsg: null,
      errorMsg: null,
      customer: null
    }
    this.startForm();
  }

  modalSuccess = (presale: RPresale) => {
    this.modalService.dismissAll('Created');
    this.router.navigate(['presales', presale.id]);
  }

  modalError = (err) => {
    this.modal.errorMsg = this.errorMessageService.parseError(err['error']);
  }

  closeAlert(alert: MAlert) {
    const i = this.alerts.indexOf(alert);
    if (i >= 0) {
      this.alerts.splice(i, 1);
    }
  }

  pageChange(selected: number) {
    this.pageNumber = selected;
    this.listPresales();
  }

  selectCustomer(customer: RCustomer) {
    if (customer != null && customer.id != null) {
      this.modal.customer = customer;
      this.presaleForm.get('customerId').setValue(customer.id);
    }
  }

  deselectCustomer() {
    this.modal.customer = null;
    this.presaleForm.get('customerId').setValue(null);
    this.presaleForm.get('customerSelectedId').setValue(null);
  }

  autocompleteFormater = (customer: RCustomer) => {
    return '(' + customer.cuit + ') ' + customer.name;
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  receiveCollapsed($event) {
    this.collapedSideBar = $event;
  }

}

export class ModalPresale {
  presale: RPresale;
  successMsg: string;
  errorMsg: string;
  customer: RCustomer;
}

export class Filter {
  customer: RCustomer;
  tmpCustomer: RCustomer;
  statuses: Array<string>;
}