import { Component, OnInit } from '@angular/core';
import { RUser } from '../../entity/ruser';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { UserService } from 'src/app/service/user.service'
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ErrorMessageService } from '../../service/error-message.service';
import { AuthorizationService } from '../../service/authorization.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['../layout.scss']
})
export class UsersComponent implements OnInit {

  collapedSideBar: boolean;

  closeResult: string;

  users: Array<RUser> = [];

  modal: ModalUser = {
    user: <RUser>{},
    successMsg: null,
    errorMsg: null
  }

  searchInput: FormControl;

  userForm: FormGroup;

  constructor(
    private authorizationService: AuthorizationService,
    private userService: UserService,
    private errorMessageService: ErrorMessageService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.searchInput = new FormControl();
    this.searchInput.valueChanges
      .pipe(debounceTime(200))
      .subscribe(name => {
        if (name == null || name == '') {
          this.users = [];
        } else {
          this.searchUsers(name);
        }
      });
  }

  private startForm() {
    this.userForm = new FormGroup({
      firstName: new FormControl(this.modal.user.firstName, [
        Validators.required,
        Validators.minLength(3)
      ]),
      lastName: new FormControl(this.modal.user.lastName, [
        Validators.required,
        Validators.minLength(3)
      ]),
      email: new FormControl(this.modal.user.email, [
        Validators.required,
        Validators.email
      ]),
      role: new FormControl(this.modal.user.role, [
        Validators.required
      ])
    })
  }

  private searchUsers(name: string) {
    this.userService.list(0, name, 'lastName', false).subscribe(page => {
      this.users = page.content;
    });
  }

  receiveCollapsed($event) {
    this.collapedSideBar = $event;
  }

  create(content) {
    this.createModal(<RUser>{});
    this.open(content);
  }

  edit(content, user: RUser) {
    this.createModal(user);
    this.open(content);
  }

  delete(content, user: RUser) {
    this.createModal(user);
    this.open(content);
  }

  open(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.close();
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.close();
    });
  }

  save() {
    const nu: RUser = {
      id: this.modal.user.id,
      email: this.userForm.get('email').value,
      firstName: this.userForm.get('firstName').value,
      lastName: this.userForm.get('lastName').value,
      password: null,
      role: this.userForm.get('role').value,
      token: null
    }
    if (nu.id == null) {
      this.userService.create(nu).subscribe(this.updateUser, this.errorUser);
    } else {
      this.userService.edit(nu.id, nu).subscribe(this.updateUser, this.errorUser);
    }
  }

  deleteUser() {
    this.userService.delete(this.modal.user.id).subscribe(res => {
      const i = this.users.indexOf(this.modal.user);
      if (i >= 0) {
        this.users.splice(i, 1);
      }
      this.modalService.dismissAll('deleted');
    }, this.errorUser);
  }

  private createModal(user: RUser) {
    this.modal = {
      user: user,
      successMsg: null,
      errorMsg: null
    }
    this.startForm();
  }

  private updateUser = (res: RUser) => {
    let u = this.modal.user;
    const isNew = u.id == null;
    u.id = res.id;
    u.lastName = res.lastName;
    u.firstName = res.firstName;
    u.email = res.email;
    u.role = res.role;
    u.token = res.token;
    u.password = null;
    this.modal.successMsg = "Guardado";
    if (isNew) {
      this.users.push(u);
    }
  }

  private errorUser = (err) => {
    this.modal.errorMsg = this.errorMessageService.parseError(err['error']);
  }

  private close() {
    this.createModal(<RUser>{});
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  isInvalidField(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  isRoleEditable(user: RUser): boolean {
    const sameUser = this.isSameUser(user);
    const notSuper = user.role != 'ADMIN';
    const res = !sameUser && notSuper;
    return res;
  }

  isSameUser(user: RUser): boolean {
    const self: RUser = this.authorizationService.getLogedIn();
    return self.id == user.id;
  }

}

export class ModalUser {
  user: RUser;
  successMsg: string;
  errorMsg: string;
}
