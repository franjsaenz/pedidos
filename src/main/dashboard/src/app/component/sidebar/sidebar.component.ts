import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthorizationService } from '../../service/authorization.service';
import { RUser } from '../../entity/ruser';
import { ModalProfileComponent } from '../modal-profile/modal-profile.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  isActive: boolean = false;

  collapsed: boolean = false;

  pushRightClass: string = 'push-right';

  showMenu: string = '';

  user: RUser;

  @Output() collapsedEvent = new EventEmitter<boolean>();

  constructor(
    private authorizationService: AuthorizationService,
    private router: Router,
    private modalService: NgbModal
    ) {
    this.router.events.subscribe(val => {
      if (
        val instanceof NavigationEnd &&
        window.innerWidth <= 992 &&
        this.isToggled()
      ) {
        this.toggleSidebar();
      }
    });
  }

  ngOnInit() {
    this.user = this.authorizationService.getLogedIn();
  }

  eventCalled() {
    this.isActive = !this.isActive;
  }

  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }

  isToggled(): boolean {
    const dom: Element = document.querySelector('body');
    return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
    const dom: any = document.querySelector('body');
    dom.classList.toggle(this.pushRightClass);
  }

  toggleCollapsed() {
    this.collapsed = !this.collapsed;
    this.collapsedEvent.emit(this.collapsed);
  }

  openProfile() {
    this.modalService.open(ModalProfileComponent, { size: 'lg' });
  }

  logout() {
    this.authorizationService.logout();
    this.router.navigate(['/']);
  }

}
