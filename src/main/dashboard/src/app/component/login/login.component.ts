import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PLogin } from '../../entity/plogin';
import { PublicService } from '../../service/public.service';
import { AuthorizationService } from '../../service/authorization.service';
import { ErrorMessageService } from '../../service/error-message.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { RShop } from '../../entity/rshop';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  isLogedin: boolean = false;

  isUploading: boolean = false;

  errorMessage: string = null;

  shops: Array<RShop> = null;

  constructor(private authorizationService: AuthorizationService,
    private errorMessageService: ErrorMessageService,
    private publicService: PublicService,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.isLogedin = this.authorizationService.isLogin();
    if (this.isLogedin) {
      this.router.navigate(['/presales']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required
      ])
    });
  }

  submit(shop?: RShop) {
    console.log(shop);
    if (this.isUploading) {
      return;
    }
    let login: PLogin = this.fetchForm();
    this.isUploading = true;
    this.clearError();
    this.publicService.login(login, shop).subscribe(user => {
      this.authorizationService.login(user);
      this.isLogedin = true;
      this.isUploading = false;
      switch (user.role) {
        case 'SUPER':
          this.router.navigate(['/shops']);
          break;
        case 'ADMIN':
          this.router.navigate(['/users']);
          break;
        default:
          this.router.navigate(['/presales']);
          break;
      }
    }, (err : HttpErrorResponse) => {
      this.isUploading = false;
      if (err.error.shops) {
        this.shops = err.error.shops;
      } else {
        this.errorMessage = this.errorMessageService.parseError(err['error']);
      }
    });
  }

  private fetchForm(): PLogin {
    return {
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value
    };
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  clearError() {
    this.errorMessage = null;
  }

}
