import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { RStat, RStatusMonthly } from '../../entity/rstat';
import { RCustomer } from '../../entity/rcustomer';
import { RUser } from '../../entity/ruser';
import { ShopService } from '../../service/shop.service';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['../layout.scss']
})
export class StatsComponent implements OnInit {

  public lineChartOptions = environment.chartOptions;

  public lineChartColors = environment.chartColors;

  stats: RStat = null;

  /* stats: RStat = {
    byStatus: {
      pending: { month: 5, year: 8 },
      sent: { month: 9, year: 21 },
      deliver: { month: 21, year: 91 },
      cancel: { month: 1, year: 18 }
    },
    byCustomer: [
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 },
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 },
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 },
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 },
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 },
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 },
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 },
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 },
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 },
      { customer: <RCustomer>{ name: 'Nombre', cuit: '11-22333444-5', balance: -129.28 }, total: 15 }
    ],
    byUser: [
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 },
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 },
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 },
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 },
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 },
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 },
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 },
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 },
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 },
      { user: <RUser>{ firstName: 'Nombre', lastName: 'Apellido' }, total: 10 }
    ],
    monthly: null
  } */

  collapedSideBar: boolean;

  constructor(private shopService: ShopService) { }

  ngOnInit() {
    this.shopService.getStats().subscribe(res => {
      this.stats = res;
    }, err => {
      console.error(err);
    });
  }

  fillMonthly() {
    let m = [];
    m.push(this.fill('Pendientes'));
    m.push(this.fill('Enviados'));
    m.push(this.fill('Entregados'));
    m.push(this.fill('Cancelados'));
    let l = ['11/17', '12/17', '01/18', '02/18', '03/18', '04/18', '05/18', '06/18', '07/18', '08/18', '09/18', '10/18'];
    let mon: RStatusMonthly = {
      data: m,
      labels: l
    }
    this.stats.monthly = mon;
  }

  private fill(label) {
    let d = [];
    for (let i = 0; i < 12; i++) {
      d.push(Math.floor((Math.random() * 10) + 1));
    }
    return { data: d, label: label };
  }

  receiveCollapsed($event) {
    this.collapedSideBar = $event;
  }

}
