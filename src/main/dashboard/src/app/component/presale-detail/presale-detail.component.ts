import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { PresaleService } from '../../service/presale.service';
import { RPresale } from '../../entity/rpresale';
import { MAlert } from '../../entity/malert';
import { ErrorMessageService } from '../../service/error-message.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AddressService } from '../../service/address.service';
import { RAddress } from '../../entity/raddress';
import { NgbDate, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-presale-detail',
  templateUrl: './presale-detail.component.html',
  styleUrls: ['../layout.scss']
})
export class PresaleDetailComponent implements OnInit {

  collapedSideBar: boolean;

  loadingAlert: MAlert = { type: 'primary', message: 'Cargando el pedido...', dismissible: false };

  alerts: Array<MAlert> = [];

  presale: RPresale;

  detailsSize: number = 0;

  newStatus: string;

  today: NgbDate;

  deliverDate: NgbDate;

  selectedDeliverDate: NgbDate;

  inputedShippingPrice: number;

  addressForm: FormGroup;

  constructor(
    private presaleService: PresaleService,
    private addressService: AddressService,
    private errorMessageService: ErrorMessageService,
    private route: ActivatedRoute,
    private ngbCalendar: NgbCalendar
  ) { }

  ngOnInit() {
    this.alerts.push(this.loadingAlert);
    this.presale = null;
    this.today = this.ngbCalendar.getToday();
    this.route.paramMap.pipe(
      switchMap(param => {
        const c = param.get('id');
        return this.presaleService.get(c);
      })
    ).subscribe(this.presaleLoaded, this.presaleError);
  }

  private startAddressForm() {
    this.addressForm = new FormGroup({
      'street': new FormControl(this.presale.address.street, [
        Validators.required,
        Validators.minLength(2)
      ]),
      'number': new FormControl(this.presale.address.number, [
        Validators.pattern('^[0-9]*$')
      ]),
      'district': new FormControl(this.presale.address.district, [
        Validators.required,
        Validators.minLength(3)
      ]),
      'latitude': new FormControl(this.presale.address.latitude, [
        Validators.pattern('^[-+]?([0-9]{1,3})(\.[0-9]*)?$'),
        Validators.max(90.0),
        Validators.min(-90.0)
      ]),
      'longitude': new FormControl(this.presale.address.longitude, [
        Validators.pattern('^[-+]?([0-9]{1,3})(\.[0-9]*)?$'),
        Validators.max(180.0),
        Validators.min(-180.0)
      ])
    })
    if (this.presale.status != 'PENDING') {
      this.addressForm.disable();
    }
  }

  private presaleLoaded = (p: RPresale) => {
    this.closeAlert(this.loadingAlert);
    this.reloadPresale(p);
  }

  private presaleError = (err) => {
    this.closeAlert(this.loadingAlert);
    const msg = this.errorMessageService.parseError(err['error']);
    this.alerts.push({ type: 'warning', message: msg, dismissible: false });
  }

  getNewDetailsSize(newSize: number) {
    this.detailsSize = newSize;
  }

  saveAddress() {
    let na: RAddress = {
      id: this.presale.address.id,
      street: this.addressForm.get('street').value,
      number: this.addressForm.get('number').value,
      district: this.addressForm.get('district').value,
      latitude: this.addressForm.get('latitude').value,
      longitude: this.addressForm.get('longitude').value
    }
    if (na.id == this.presale.customer.address.id) {
      this.addressService.create(na).subscribe(this.addressUpdated, this.updateError);
    } else {
      this.addressService.update(na.id, na).subscribe(this.addressUpdated, this.updateError);
    }
  }

  deleteAddress() {
    if (this.presale.customer.address.id == this.presale.address.id) {
      return;
    }
    this.addressService.delete(this.presale.address.id).subscribe(res => { });
    this.addressUpdated(this.presale.customer.address);
  }

  resetAddress() {
    this.startAddressForm();
  }

  private addressUpdated = (a: RAddress) => {
    this.presale.address = a;
    this.presale.addressId = a.id;
    this.presaleService.update(this.presale.id, this.presale).subscribe(this.presaleUpdated, this.updateError);
  }

  updateStatus(status: string) {
    this.newStatus = status;
  }

  confirmStatus() {
    this.presale.status = this.newStatus;
    this.addressForm.disable();
    this.presaleService.update(this.presale.id, this.presale).subscribe(this.presaleUpdated, this.updateError);
  }

  changeDeliveryDate(selected: NgbDate) {
    this.selectedDeliverDate = selected;
  }

  confirmDeliveryDate() {
    let sdd = this.selectedDeliverDate;
    const sddm = sdd.month < 10 ? '0' + sdd.month : '' + sdd.month;
    const sddd = sdd.day < 10 ? '0' + sdd.day : '' + sdd.day;
    let newDeliveryDate: string = sdd.year + '-' + sddm + '-' + sddd;
    this.presale.deliverDate = newDeliveryDate;
    this.presaleService.update(this.presale.id, this.presale).subscribe(this.presaleUpdated, this.updateError);
  }

  updateShippingPrice() {
    this.presale.shippingPrice = this.inputedShippingPrice;
    this.presaleService.update(this.presale.id, this.presale).subscribe(this.presaleUpdated, this.updateError);
  }

  private presaleUpdated = (p: RPresale) => {
    this.reloadPresale(p);
    this.alerts.push({ type: 'success', message: 'Información actualizada', dismissible: true });
  }

  private updateError = (err) => {
    const msg = this.errorMessageService.parseError(err['error']);
    this.alerts.push({ type: 'warning', message: msg, dismissible: true });
  }

  private reloadPresale(p: RPresale) {
    this.newStatus = null;
    this.selectedDeliverDate = null;
    this.inputedShippingPrice = p.shippingPrice;
    this.presale = p;
    if (p.deliverDate != null) {
      let dd = new Date(p.deliverDate);
      this.deliverDate = new NgbDate(dd.getUTCFullYear(), dd.getUTCMonth() + 1, dd.getUTCDate());
    } else {
      this.deliverDate = null;
    }
    this.startAddressForm();
  }

  closeAlert(alert: MAlert) {
    const i = this.alerts.indexOf(alert);
    if (i >= 0) {
      this.alerts.splice(i, 1);
    }
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  receiveCollapsed($event) {
    this.collapedSideBar = $event;
  }

}
