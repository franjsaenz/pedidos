import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from '../../service/customer.service';
import { RCustomer } from '../../entity/rcustomer';
import { debounceTime } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ErrorMessageService } from 'src/app/service/error-message.service';
import { RAddress } from '../../entity/raddress';
import { AddressService } from '../../service/address.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['../layout.scss']
})
export class CustomersComponent implements OnInit {

  collapedSideBar: boolean;

  searchInput: FormControl;

  customerForm: FormGroup;

  customers: Array<RCustomer> = [];

  modal: ModalUser = {
    customer: <RCustomer>{},
    successMsg: null,
    errorMsg: null
  }

  constructor(
    private customerService: CustomerService,
    private addressService: AddressService,
    private errorMessageService: ErrorMessageService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.searchInput = new FormControl();
    this.searchInput.valueChanges
      .pipe(debounceTime(200))
      .subscribe(name => {
        if (name == null || name == '') {
          this.customers = [];
        } else {
          this.searchCustomers(name);
        }
      });
  }

  private startForm() {
    this.customerForm = new FormGroup({
      'cuit': new FormControl(this.modal.customer.cuit, [
        Validators.required,
        Validators.pattern('^(20|23|24|27|30|33|34)(-)[0-9]{8}(-)[0-9]$')
      ]),
      'name': new FormControl(this.modal.customer.name, [
        Validators.required,
        Validators.minLength(3)
      ]),
      'street': new FormControl(this.modal.customer.address.street, [
        Validators.required,
        Validators.minLength(2)
      ]),
      'number': new FormControl(this.modal.customer.address.number, [
        Validators.pattern('^[0-9]*$')
      ]),
      'district': new FormControl(this.modal.customer.address.district, [
        Validators.required,
        Validators.minLength(3)
      ]),
      'latitude': new FormControl(this.modal.customer.address.latitude, [
        Validators.pattern('^[-+]?([0-9]{1,3})(\.[0-9]*)?$'),
        Validators.max(90.0),
        Validators.min(-90.0)
      ]),
      'longitude': new FormControl(this.modal.customer.address.longitude, [
        Validators.pattern('^[-+]?([0-9]{1,3})(\.[0-9]*)?$'),
        Validators.max(180.0),
        Validators.min(-180.0)
      ])
    });
  }

  private searchCustomers(name: string) {
    this.customerService.list(0, name, 'name', false).subscribe(res => {
      this.customers = res.content;
    });
  }

  save(customer: RCustomer) {
    const nc: RCustomer = {
      id: customer.id,
      name: this.customerForm.get('name').value,
      cuit: this.customerForm.get('cuit').value,
      addressId: customer.address.id,
      balance: null,
      address: {
        id: customer.address.id,
        street: this.customerForm.get('street').value,
        number: this.customerForm.get('number').value,
        district: this.customerForm.get('district').value,
        latitude: this.customerForm.get('latitude').value,
        longitude: this.customerForm.get('longitude').value
      }
    };
    this.modal.successMsg = null;
    this.modal.errorMsg = null;
    this.saveAddress(nc);
  }

  private saveAddress(customer: RCustomer) {
    let a: RAddress = customer.address;
    if (a.id == null) {
      this.addressService.create(a).subscribe(res => {
        this.saveCustomer(res.id, customer);
      }, this.modalError);
    } else {
      this.addressService.update(a.id, a).subscribe(res => {
        this.saveCustomer(res.id, customer);
      }, this.modalError);
    }
  }

  private saveCustomer(addressId: string, customer: RCustomer) {
    customer.addressId = addressId;
    if (customer.id == null) {
      this.customerService.create(customer).subscribe(this.modalSuccess, this.modalError);
    } else {
      this.customerService.edit(customer.id, customer).subscribe(this.modalSuccess, this.modalError);
    }
  };

  deleteCustomer(customer: RCustomer) {
    this.customerService.delete(customer.id).subscribe(res => {
      let i = this.customers.indexOf(customer);
      if (i >= 0) {
        this.customers.splice(i, 1);
      }
      this.modalService.dismissAll('deleted');
    });
  }

  openNewModal(content) {
    this.createModal(<RCustomer>{ address: {} });
    this.openModal(content);
  }

  openEditModal(content, customer: RCustomer) {
    this.createModal(customer);
    this.openModal(content);
  }

  openDeleteModal(content, customer: RCustomer) {
    this.createModal(customer);
    this.openModal(content);
  }

  openModal(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeModal();
    }, (reason) => {
      this.closeModal();
    });
  }

  private closeModal() {
    this.createModal(<RCustomer>{ address: {} });
  }

  private createModal(customer: RCustomer) {
    this.modal = {
      customer: customer,
      successMsg: null,
      errorMsg: null
    }
    this.startForm();
  }

  private updateModal(customer: RCustomer) {
    this.modal.customer.id = customer.id;
    this.modal.customer.name = customer.name;
    this.modal.customer.balance = customer.balance;
    this.modal.customer.addressId = customer.addressId;
    this.modal.customer.address = customer.address;
  }

  modalSuccess = (customer: RCustomer) => {
    let i = this.customers.indexOf(this.modal.customer);
    if (i < 0) {
      this.customers.push(customer);
    }
    this.updateModal(customer);
    this.modal.successMsg = 'Guardado';
  }

  modalError = (err) => {
    this.modal.errorMsg = this.errorMessageService.parseError(err['error']);
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  receiveCollapsed($event) {
    this.collapedSideBar = $event;
  }

}

export class ModalUser {
  customer: RCustomer;
  successMsg: string;
  errorMsg: string;
}
