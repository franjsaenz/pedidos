import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../../service/authorization.service';
import { Router } from '@angular/router';
import { RUser } from '../../entity/ruser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalProfileComponent } from '../modal-profile/modal-profile.component';
import { ShopService } from '../../service/shop.service';
import { RShop } from '../../entity/rshop';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  pushRightClass: string = 'push-right';

  user: RUser;

  shop: RShop;

  constructor(
    private shopService: ShopService,
    private authorizationService: AuthorizationService,
    private router: Router,
    private modalService: NgbModal
  ) {
    if (!this.authorizationService.isLogin()) {
      this.logout();
    }
  }

  ngOnInit() {
    this.user = this.authorizationService.getLogedIn();
    if (this.user.role != 'SUPER') {
      this.shopService.getSelf().subscribe(res => {
        this.shop = res;
      });
    }
  }

  isToggled(): boolean {
    const dom: Element = document.querySelector('body');
    return dom.classList.contains(this.pushRightClass);
  }

  toggleSidebar() {
    console.log('Toggling');
    const dom: any = document.querySelector('body');
    dom.classList.toggle(this.pushRightClass);
  }

  openProfile() {
    this.modalService.open(ModalProfileComponent, { size: 'lg' });
  }

  logout() {
    this.authorizationService.logout();
    this.router.navigate(['/']);
  }

}
