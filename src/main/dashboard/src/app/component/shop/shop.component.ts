import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RShop } from '../../entity/rshop';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ShopService } from '../../service/shop.service';
import { MAlert } from '../../entity/malert';
import { ErrorMessageService } from '../../service/error-message.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RUser } from '../../entity/ruser';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['../layout.scss']
})
export class ShopComponent implements OnInit {

  collapedSideBar: boolean;

  alerts: Array<MAlert> = [];

  searchInput: FormControl;

  shopForm: FormGroup;

  pageNumber: number = 1;

  pageSize: number = environment.pageSize;

  totalPages: number = 0;

  totalElements: number = 0;

  shops: Array<RShop> = [];

  modal: Modal;

  constructor(
    private shopService: ShopService,
    private errorMessageService: ErrorMessageService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.searchInput = new FormControl();
    this.searchInput.valueChanges
      .pipe(debounceTime(200))
      .subscribe(name => {
        this.searchShops();
      });
    this.searchShops();
  }

  private startForm() {
    let form = {
      'name': new FormControl(this.modal.shop.name, [
        Validators.required,
        Validators.minLength(3)
      ])
    }
    if (this.modal.shop.id == null) {
      form['firstName'] = new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]);
      form['lastName'] = new FormControl('', [
        Validators.required,
        Validators.minLength(3)
      ]);
      form['email'] = new FormControl('', [
        Validators.required,
        Validators.email
      ]);
    }
    console.log(form);
    this.shopForm = new FormGroup(form);
  }

  private searchShops() {
    let page = this.pageNumber - 1;
    let name = this.searchInput.value;
    this.shopService.list(page, name, 'name', true).subscribe(
      res => {
        this.shops = res.content;
        this.totalPages = res.totalPages;
        this.totalElements = res.totalElements;
      }, err => {
        const msg = this.errorMessageService.parseError(err['error']);
        this.alerts.push({ type: 'warning', message: msg, dismissible: true });
      }
    );
  }

  save(shop: RShop) {
    const ns: RShop = {
      id: shop.id,
      created: shop.created,
      name: this.shopForm.get('name').value,
      friendlyName: null,
      user: shop.id != null ? null : {
        id: null,
        email: this.shopForm.get('email').value,
        firstName: this.shopForm.get('firstName').value,
        lastName: this.shopForm.get('lastName').value,
        password: null,
        role: null,
        token: null
      }
    }
    console.log(ns);
    this.modal.alers = [];
    if (ns.id == null) {
      this.shopService.create(ns).subscribe(this.modalSuccess, this.modalError);
    } else {
      this.shopService.edit(ns.id, ns).subscribe(this.modalSuccess, this.modalError);
    }
  }

  private updateModal(shop: RShop) {
    this.modal.shop.id = shop.id;
    this.modal.shop.name = shop.name;
    this.modal.shop.friendlyName = shop.friendlyName;
  }

  modalSuccess = (shop: RShop) => {
    let i = this.shops.indexOf(this.modal.shop);
    if (i < 0) {
      this.shops.push(shop);
    }
    this.updateModal(shop);
    this.modalService.dismissAll('Created');
  }

  modalError = (err) => {
    let errorMsg = this.errorMessageService.parseError(err['error']);
    this.modal.alers.push({ dismissible: true, message: errorMsg, type: 'warning' });
  }

  openNewModal(content) {
    this.createModal(<RShop>{});
    this.openModal(content);
  }

  openEditModal(content, shop: RShop) {
    this.createModal(shop);
    this.openModal(content);
  }

  openModal(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeModal();
    }, (reason) => {
      this.closeModal();
    });
  }

  private closeModal() {
    this.createModal(<RShop>{});
  }

  private createModal(shop: RShop) {
    this.modal = {
      shop: shop,
      alers: []
    }
    this.startForm();
  }

  pageChange(selected: number) {
    this.pageNumber = selected;
    this.searchShops();
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  receiveCollapsed($event) {
    this.collapedSideBar = $event;
  }

}

export class Modal {
  shop: RShop;
  alers: Array<MAlert>;
}