import { Component, OnInit } from '@angular/core';
import { RProduct } from '../../entity/rproduct';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../service/product.service';
import { CategoryService } from '../../service/category.service';
import { ErrorMessageService } from '../../service/error-message.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { debounceTime } from 'rxjs/operators';
import { RouterPreloader } from '@angular/router';
import { RCategory } from '../../entity/rcategory';
import { RPhoto } from '../../entity/rphoto';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['../layout.scss']
})
export class ProductsComponent implements OnInit {

  collapedSideBar: boolean;

  searchInput: FormControl;

  productForm: FormGroup;

  products: Array<RProduct> = [];

  modal: ModalProduct = {
    product: <RProduct>{},
    photo: null,
    photoB64: null,
    successMsg: null,
    errorMsg: null,
    category: null
  };

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService,
    private errorMessageService: ErrorMessageService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.searchInput = new FormControl();
    this.searchInput.valueChanges
      .pipe(debounceTime(200))
      .subscribe(name => {
        if (name == null || name == '') {
          this.products = [];
        } else {
          this.searchProducts(name);
        }
      });
  }

  private startForm() {
    this.productForm = new FormGroup({
      'code': new FormControl(this.modal.product.code, [
        Validators.required
      ]),
      'name': new FormControl(this.modal.product.name, [
        Validators.required,
        Validators.minLength(3)
      ]),
      'purchasePrice': new FormControl(this.modal.product.purchasePrice, [
        Validators.required,
        Validators.pattern('^[0-9]+(\.[0-9]*)?$'),
        Validators.min(0)
      ]),
      'salePrice': new FormControl(this.modal.product.salePrice, [
        Validators.required,
        Validators.pattern('^[0-9]+(\.[0-9]*)?$'),
        Validators.min(0)
      ]),
      'tax': new FormControl(this.modal.product.tax, [
        Validators.required,
        Validators.pattern('^[0-9]+(\.[0-9]*)?$'),
        Validators.min(0)
      ]),
      'categoryId': new FormControl(this.modal.product.categoryId, [
        Validators.required
      ]),
      'categorySelectedId': new FormControl()
    });
  }

  private searchProducts(name: string) {
    this.productService.list(0, name, 'name', false, null).subscribe(res => {
      this.products = res.content;
    });
  }

  public filterObservableCategories = (text: string) => {
    return this.categoryService.list(0, text, "name", false, false);
  }

  save(product: RProduct) {
    const np: RProduct = {
      id: product.id,
      name: this.productForm.get('name').value,
      code: this.productForm.get('code').value,
      purchasePrice: this.productForm.get('purchasePrice').value,
      salePrice: this.productForm.get('salePrice').value,
      tax: this.productForm.get('tax').value,
      categoryId: this.productForm.get('categoryId').value,
      category: null
    };
    if (np.id == null) {
      this.productService.create(np).subscribe(this.modalSuccess, this.modalError);
    } else {
      this.productService.update(np.id, np).subscribe(this.modalSuccess, this.modalError);
    }
  }

  handlePhoto(photo: File[]) {
    if (photo != null && photo.length > 0) {
      let file: File = photo[0];
      if (file.type.indexOf('image') < 0) {
        return;
      }
      this.modal.photo = file;
      const fileReader: FileReader = new FileReader();
      fileReader.onload = () => {
        this.modal.photoB64 = <string>fileReader.result;
      }
      fileReader.readAsDataURL(file);
    }
  }

  removePhoto() {
    if (this.modal.photo != null) {
      this.modal.photo = null;
      this.modal.photoB64 = this.modal.product.photo;
    } else if (this.modal.product.id != null) {
      this.productService.delPhoto(this.modal.product.id).subscribe();
      this.modal.photo = null;
      this.modal.photoB64 = null;
      this.modal.product.photo = null;
    }
  }

  deleteProduct(product: RProduct) {
    this.productService.delete(product.id).subscribe(res => {
      let i = this.products.indexOf(product);
      if (i >= 0) {
        this.products.splice(i, 1);
      }
      this.modalService.dismissAll('deleted');
    }, this.modalError);
  }

  openNewModal(content) {
    this.createModal(<RProduct>{});
    this.openModal(content);
  }

  openEditModal(content, product: RProduct) {
    this.createModal(product);
    this.openModal(content);
  }

  openDeleteModal(content, product: RProduct) {
    this.createModal(product);
    this.openModal(content);
  }

  openModal(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeModal();
    }, (reason) => {
      this.closeModal();
    });
  }

  private closeModal() {
    this.createModal(<RProduct>{});
  }

  private createModal(product: RProduct) {
    this.modal = {
      product: product,
      photo: null,
      photoB64: product.photo != null ? product.photo : null,
      successMsg: null,
      errorMsg: null,
      category: product.category
    }
    this.startForm();
  }

  private updateModal(product: RProduct) {
    this.modal.product.id = product.id;
    this.modal.product.name = product.name;
    this.modal.product.code = product.code;
    this.modal.product.purchasePrice = product.purchasePrice;
    this.modal.product.salePrice = product.salePrice;
    this.modal.product.tax = product.tax;
    this.modal.product.categoryId = product.categoryId;
    this.modal.product.category = product.category;
  }

  modalSuccess = (product: RProduct) => {
    let i = this.products.indexOf(this.modal.product);
    if (i < 0) {
      this.products.push(product);
    }
    this.updateModal(product);
    if (this.modal.photo != null) {
      const b64 = this.modal.photoB64.split(',');
      let p: RPhoto = {
        name: this.modal.photo.name,
        base64: b64[1],
        meta: b64[0]
      }
      this.productService.addPhoto(product.id, p).subscribe(
        res => {
          product.photo = res.message;
          this.modalService.dismissAll('Created');
        }, this.modalError
      );
    } else {
      this.modalService.dismissAll('Created');
    }
  }

  modalError = (err) => {
    this.modal.errorMsg = this.errorMessageService.parseError(err['error']);
  }

  selectCategory(category: RCategory) {
    if (category != null && category.id != null) {
      this.modal.category = category;
      this.productForm.get('categoryId').setValue(category.id);
    }
  }

  deselectCategory() {
    this.modal.category = null;
    this.productForm.get('categoryId').setValue(null);
    this.productForm.get('categorySelectedId').setValue(null);
  }

  autocompleteFormater = (category: RCategory) => {
    return category.code + ' - ' + category.name;
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  receiveCollapsed($event) {
    this.collapedSideBar = $event;
  }

}

export class ModalProduct {
  product: RProduct;
  photo: File;
  photoB64: string;
  successMsg: string;
  errorMsg: string;
  category: RCategory;
}
