import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PresaleService } from '../../service/presale.service';
import { RPresaleDetail } from '../../entity/rpresale-detail';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RProduct } from '../../entity/rproduct';
import { ProductService } from '../../service/product.service';
import { RPresale } from '../../entity/rpresale';
import { environment } from 'src/environments/environment';
import { MAlert } from '../../entity/malert';
import { ErrorMessageService } from '../../service/error-message.service';

@Component({
  selector: 'app-presale-detail-details',
  templateUrl: './presale-detail-details.component.html'
})
export class PresaleDetailDetailsComponent implements OnInit {

  @Input() presale: RPresale;

  loadingAlert: MAlert = { type: 'primary', message: 'Cargando los detalles...', dismissible: false };

  loadingDetails: boolean = false;

  alerts: Array<MAlert> = [];

  newDetailFrom: FormGroup;

  newDetail: RPresaleDetail;

  toDelete: RPresaleDetail;

  pageNumber: number = 1;

  pageSize: number = environment.pageSize;

  totalPages: number = 0;

  totalElements: number = 0;

  details: Array<RPresaleDetail> = [];

  @Output() detailsSize = new EventEmitter<number>();

  constructor(
    private presaleService: PresaleService,
    private productService: ProductService,
    private errorMessageService: ErrorMessageService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.startNewDetail(<RPresaleDetail>{});
    this.listDetails();
  }

  private listDetails() {
    this.loadingDetails = true;
    this.alerts.push(this.loadingAlert);
    let page = this.pageNumber - 1;
    this.presaleService.listDetails(this.presale, page, 'created', false).subscribe(res => {
      this.loadingDetails = false;
      this.closeAlert(this.loadingAlert);
      this.totalPages = res.totalPages;
      this.totalElements = res.totalElements;
      this.details = res.content;
      this.notifyNewSize();
    }, err => {
      this.loadingDetails = false;
      this.closeAlert(this.loadingAlert);
      const msg = this.errorMessageService.parseError(err['error']);
      this.alerts.push({ type: 'warning', message: msg, dismissible: true });
    })
  }

  private notifyNewSize() {
    this.detailsSize.emit(this.details.length);
  }

  public filterObservableProducts = (text: string) => {
    return this.productService.list(0, text, 'name', false, null);
  }

  startNewDetail(d: RPresaleDetail) {
    this.newDetail = d;
    this.newDetailFrom = new FormGroup({
      'productId': new FormControl(this.newDetail.productId, [
        Validators.required
      ]),
      'amount': new FormControl(this.newDetail.amount, [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
      ]),
      'discount': new FormControl(this.newDetail.discount, [
        Validators.pattern('^[0-9]+(\.[0-9]*)?$'),
        Validators.min(0)
      ])
    });
  }

  selectProduct(product: RProduct) {
    if (product == null || product.id == null) {
      this.newDetail.product = null;
      this.newDetail.productId = null;
      this.newDetailFrom.get('productId').setValue(null);
    } else {
      this.newDetail.product = product;
      this.newDetail.productId = product.id;
      this.newDetailFrom.get('productId').setValue(product.id);
    }
  }

  saveDetail() {
    this.newDetail.amount = this.newDetailFrom.get('amount').value;
    this.newDetail.discount = this.newDetailFrom.get('discount').value;
    this.newDetailFrom.disable();
    if (this.newDetail.id == null) {
      this.presaleService.createDetail(this.presale, this.newDetail).subscribe(this.updateDetail, this.errorDetail);
    } else {
      this.presaleService.updateDetail(this.presale, this.newDetail).subscribe(this.updateDetail, this.errorDetail);
    }
  }

  deleteDetail(d: RPresaleDetail) {
    this.toDelete = d;
  }

  confirmDelete() {
    let d = this.toDelete;
    this.presaleService.deleteDetail(this.presale, d).subscribe(res => {
      let i = this.details.indexOf(d);
      if (i >= 0) {
        this.details.splice(i, 1);
      }
      this.notifyNewSize();
      this.presale.subtotal -= d.subtotal;
      this.presale.total -= d.subtotal;
      this.startNewDetail(<RPresaleDetail>{});
      this.toDelete = null;
    }, this.errorDetail);
  }

  private updateDetail = (d: RPresaleDetail) => {
    if (this.newDetail.id == null) {
      this.details.push(d);
      this.notifyNewSize();
    } else {
      let o = this.newDetail;
      this.presale.subtotal -= o.subtotal;
      this.presale.total -= o.subtotal;
      o.id = d.id;
      o.amount = d.amount;
      o.discount = d.discount;
      o.product = d.product;
      o.productId = d.productId;
      o.subtotal = d.subtotal;
    }
    this.presale.subtotal += d.subtotal;
    this.presale.total += d.subtotal;
    this.newDetailFrom.enable();
    this.startNewDetail(<RPresaleDetail>{});
  }

  private errorDetail = (err) => {
    const msg = this.errorMessageService.parseError(err['error']);
    this.alerts.push({ type: 'warning', message: msg, dismissible: true });
    this.newDetailFrom.enable();
  }

  closeAlert(alert: MAlert) {
    const i = this.alerts.indexOf(alert);
    if (i >= 0) {
      this.alerts.splice(i, 1);
    }
  }

  pageChange(selected: number) {
    this.pageNumber = selected;
    this.listDetails();
  }

  autocompleteFormater = (product: RProduct) => {
    return '(' + product.code + ') ' + product.name;
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

}
