import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../../service/authorization.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { RUser } from '../../entity/ruser';
import { UserService } from '../../service/user.service';
import { ErrorMessageService } from '../../service/error-message.service';
import { MAlert } from '../../entity/malert';
import { RUpdatePassword } from '../../entity/rupdate-password';

@Component({
  selector: 'app-modal-profile',
  templateUrl: './modal-profile.component.html',
  styleUrls: ['../layout.scss']
})
export class ModalProfileComponent implements OnInit {

  user: RUser;

  alert: MAlert;

  profileForm: FormGroup;

  passwordForm: FormGroup;

  constructor(
    private authorizationService: AuthorizationService,
    private userService: UserService,
    private errorMessageService: ErrorMessageService,
    private activeModal: NgbActiveModal
  ) { }

  ngOnInit() {
    this.alert = {
      type: 'primary',
      dismissible: false,
      message: 'Cargando el usuario...'
    }
    const u: RUser = this.authorizationService.getLogedIn();
    this.userService.get(u.id).subscribe(this.reciebeUser, this.errorUser);
    this.startPasswordForm();
  }

  private reciebeUser = (user: RUser) => {
    this.closeAlert();
    if (this.user != null) {
      this.alert = {
        type: 'success',
        message: 'Usuario actualizado',
        dismissible: true
      }
    }
    this.startForm(user);
  }

  private errorUser = (err) => {
    const msgErr: string = this.errorMessageService.parseError(err['error']);
    this.alert = {
      type: 'warning',
      dismissible: false,
      message: msgErr
    }
  }

  private startForm(user: RUser) {
    this.profileForm = new FormGroup({
      email: new FormControl({ value: user.email, disabled: true }),
      firstName: new FormControl(user.firstName, [
        Validators.required,
        Validators.minLength(3)
      ]),
      lastName: new FormControl(user.lastName, [
        Validators.required,
        Validators.minLength(3)
      ])
    });
    this.user = user;
  }

  private startPasswordForm() {
    this.passwordForm = new FormGroup({
      current: new FormControl('', [
        Validators.required
      ]),
      newPassword: new FormControl('', [
        Validators.required
      ]),
      repeat: new FormControl('', [
        Validators.required
      ])
    }, { validators: this.passwordConfirming })
  }

  save() {
    let nu: RUser = {
      id: this.user.id,
      firstName: this.profileForm.get('firstName').value,
      lastName: this.profileForm.get('lastName').value,
      email: this.user.email,
      role: this.user.role,
      password: null,
      token: null,
    }
    this.userService.edit(nu.id, nu).subscribe(this.userSaved, this.errorUser);
  }

  private userSaved = (user: RUser) => {
    this.closeAlert();
    if (this.user != null) {
      this.alert = {
        type: 'success',
        message: 'Usuario actualizado',
        dismissible: true
      }
    }
    this.startForm(user);
    this.authorizationService.updateLogedIn(user);
  }

  updatePassword() {
    let np: RUpdatePassword = {
      oldPassword: this.passwordForm.get('current').value,
      newPassword: this.passwordForm.get('newPassword').value,
      resetToken: null
    }
    this.userService.updatePassword(np).subscribe(res => {
      this.alert = {
        type: 'success',
        dismissible: true,
        message: 'Password actualizado correctamente'
      };
      this.startPasswordForm();
    })
  }

  closeAlert() {
    this.alert = null;
  }

  closeModal() {
    this.activeModal.dismiss();
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get('newPassword').value !== c.get('repeat').value) {
      return { invalid: true };
    }
  }

}
