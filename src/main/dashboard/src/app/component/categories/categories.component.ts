import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../service/category.service';
import { ErrorMessageService } from '../../service/error-message.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RCategory } from '../../entity/rcategory';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['../layout.scss']
})
export class CategoriesComponent implements OnInit {

  collapedSideBar: boolean;

  searchInput: FormControl;

  categoryForm: FormGroup;

  categories: Array<RCategory> = [];

  modal: ModalCategory = {
    category: <RCategory>{},
    successMsg: null,
    errorMsg: null,
    parent: null
  }

  constructor(
    private categoryService: CategoryService,
    private errorMessageService: ErrorMessageService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.searchInput = new FormControl();
    this.searchInput.valueChanges
      .pipe(debounceTime(200))
      .subscribe(name => {
        if (name == null || name == '') {
          this.categories = [];
        } else {
          this.searchCategories(name);
        }
      });
  }

  private startForm() {
    this.categoryForm = new FormGroup({
      'code': new FormControl(this.modal.category.code, [
        Validators.required
      ]),
      'name': new FormControl(this.modal.category.name, [
        Validators.required
      ]),
      'parentId': new FormControl({ value: this.modal.category.parentId, disabled: true }),
      'parentSelectedId': new FormControl()
    });
  }

  private searchCategories(name: string) {
    this.categoryService.list(0, name, 'name', false, false).subscribe(res => {
      this.categories = res.content;
    });
  }

  public filterObservableCategories = (text: string) => {
    return this.categoryService.list(0, text, "name", false, false);
  }

  save(category: RCategory) {
    let nc: RCategory = {
      id: category.id,
      code: this.categoryForm.get('code').value,
      name: this.categoryForm.get('name').value,
      childs: [],
      parentId: this.categoryForm.get('parentId').value
    }
    this.modal.successMsg = null;
    this.modal.errorMsg = null;
    if (nc.id == null) {
      this.categoryService.create(nc).subscribe(this.modalSuccess, this.modalError);
    } else {
      this.categoryService.update(nc.id, nc).subscribe(this.modalSuccess, this.modalError);
    }
  }

  deleteCategory(category: RCategory) {
    this.categoryService.delete(category.id).subscribe(res => {
      let i = this.categories.indexOf(category);
      if (i >= 0) {
        this.categories.splice(i, 1);
      }
      this.modalService.dismissAll('deleted');
    }, this.modalError);
  }

  openNewModal(content) {
    this.createModal(content, <RCategory>{});
  }

  openEditModal(content, category: RCategory) {
    this.categoryService.get(category.id, true).subscribe(c => {
      this.createModal(content, c);
    })
  }

  openDeleteModal(content, category: RCategory) {
    this.createModal(content, category);
  }

  openModal(content) {
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeModal();
    }, (reason) => {
      this.closeModal();
    });
  }

  private closeModal() {
    this.createModal(null, <RCategory>{});
  }

  private createModal(content, category: RCategory) {
    let p: RCategory = null;
    if (category.parentId != null) {
      this.categoryService.get(category.parentId, false).subscribe(p => {
        this._createModal(content, category, p);
      }, err => {
        this.modal.errorMsg = "No se encontró el padre del rubro seleccionado";
        this._createModal(content, category, null);
      });
    } else {
      this._createModal(content, category, null);
    }
  }

  private _createModal(content, category: RCategory, parent: RCategory) {
    this.modal = {
      category: category,
      successMsg: null,
      errorMsg: null,
      parent: parent
    }
    this.startForm();
    if (content != null) {
      this.openModal(content);
    }
  }

  private updateModal(category: RCategory) {
    this.modal.category.id = category.id;
    this.modal.category.name = category.name;
    this.modal.category.code = category.code;
    this.modal.category.parentId = category.parentId;
    this.modal.category.childs = category.childs;
  }

  modalSuccess = (category: RCategory) => {
    let i = this.categories.indexOf(this.modal.category);
    if (i < 0) {
      this.categories.push(category);
    }
    this.updateModal(category);
    this.modalService.dismissAll('Created');
  }

  modalError = (err) => {
    this.modal.errorMsg = this.errorMessageService.parseError(err['error']);
  }

  selectParent(parent: RCategory) {
    if (parent != null && parent.id != null) {
      this.modal.parent = parent;
      this.categoryForm.get('parentId').setValue(parent.id);
    }
  }

  deselectParent() {
    this.modal.parent = null;
    this.categoryForm.get('parentId').setValue(null);
    this.categoryForm.get('parentSelectedId').setValue(null);
  }

  autocompleteFormater = (category: RCategory) => {
    return category.code + ' - ' + category.name;
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  receiveCollapsed($event) {
    this.collapedSideBar = $event;
  }

}

export class ModalCategory {
  category: RCategory;
  successMsg: string;
  errorMsg: string;
  parent: RCategory;
}
