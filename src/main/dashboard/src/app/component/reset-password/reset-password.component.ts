import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAlert } from '../../entity/malert';
import { PublicService } from '../../service/public.service';
import { ErrorMessageService } from '../../service/error-message.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['../login/login.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  isUploading: boolean;

  passwordForm: FormGroup;

  alert: MAlert;

  constructor(
    private publicService: PublicService,
    private errorMessageService: ErrorMessageService
  ) { }

  ngOnInit() {
    this.passwordForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ])
    })
  }

  submit() {
    this.isUploading = true;
    this.publicService.resetPassword(this.passwordForm.get('email').value).subscribe(res => {
      this.isUploading = false;
      this.alert = { type: 'success', message: 'Se ha enviado un email con instrucciones para continuar.', dismissible: false }
    }, err => {
      this.isUploading = false;
      this.alert = { type: 'dark', message: this.errorMessageService.parseError(err['error']), dismissible: true }
    })
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  clearAlert() {
    this.alert = null;
  }

}
