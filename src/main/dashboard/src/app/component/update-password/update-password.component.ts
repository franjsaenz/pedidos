import { Component, OnInit } from '@angular/core';
import { MAlert } from '../../entity/malert';
import { FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';
import { PublicService } from '../../service/public.service';
import { ActivatedRoute } from '@angular/router';
import { ErrorMessageService } from '../../service/error-message.service';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RUpdatePassword } from '../../entity/rupdate-password';
import { AuthorizationService } from '../../service/authorization.service';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['../login/login.component.scss']
})
export class UpdatePasswordComponent implements OnInit {

  passwordToken: string;

  isUploading: boolean;

  passwordForm: FormGroup;

  alert: MAlert;

  constructor(
    private publicService: PublicService,
    private errorMessageService: ErrorMessageService,
    private authorizationService: AuthorizationService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap(param => {
        const c = param.get('id');
        this.passwordToken = c;
        this.startForm();
        return new Observable();
      })
    ).subscribe();
  }

  private startForm() {
    this.authorizationService.logout();
    this.passwordForm = new FormGroup({
      newPassword: new FormControl('', [
        Validators.required
      ]),
      repeat: new FormControl('', [
        Validators.required
      ])
    }, { validators: this.passwordConfirming })
  }

  submit() {
    const pwd: RUpdatePassword = {
      oldPassword: null,
      newPassword: this.passwordForm.get('newPassword').value,
      resetToken: this.passwordToken
    }
    this.publicService.updatePassword(pwd).subscribe(res => {
      this.isUploading = false;
      this.alert = { type: 'success', message: 'Se ha actualizado correctamente la contraseña.', dismissible: false }
    }, err => {
      this.isUploading = false;
      this.alert = { type: 'dark', message: this.errorMessageService.parseError(err['error']), dismissible: true }
    })
  }

  isInputInvalid(el: any): boolean {
    return el.invalid && (el.dirty || el.touched);
  }

  clearAlert() {
    this.alert = null;
  }

  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get('newPassword').value !== c.get('repeat').value) {
      return { invalid: true };
    }
  }

}
