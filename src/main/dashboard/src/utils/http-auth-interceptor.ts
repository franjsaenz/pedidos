import { Injectable } from "@angular/core";
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthorizationService } from '../app/service/authorization.service';
import { RUser } from "../app/entity/ruser";

@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {

    constructor(private authorizationService: AuthorizationService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let h = req.headers;
        h = h.set('Content-Type', 'application/json');
        const user: RUser = this.authorizationService.getLogedIn();
        if (user != null) {
            const bearer = user.token;
            h = h.set('Authorization', 'Bearer ' + bearer);
        }
        const clonedRequest = req.clone({ headers: h });
        return next.handle(clonedRequest);
    }

}
