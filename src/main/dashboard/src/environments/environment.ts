// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://localhost:8080',
  pageSize: 20,
  chartOptions: { responsive: true },
  chartColors: [
    { // pendientes
      backgroundColor: 'rgba(52, 58, 64, 0.2)',
      borderColor: '#343a40',
      pointBackgroundColor: '#343a40',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(52, 58, 64, 0.2)'
    },
    { // enviados
      backgroundColor: 'rgba(0, 123, 255, 0.2)',
      borderColor: '#007bff',
      pointBackgroundColor: '#007bff',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(0, 123, 255, 0.2)'
    },
    { // entregados
      backgroundColor: 'rgba(40, 167, 69, 0.2)',
      borderColor: '#28a745',
      pointBackgroundColor: '#28a745',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(40, 167, 69, 0.2)'
    },
    { // cancelados
      backgroundColor: 'rgba(220, 53, 69, 0.2)',
      borderColor: '#dc3545',
      pointBackgroundColor: '#dc3545',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(220, 53, 69, 0.2)'
    }
  ]
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
