export const environment = {
  production: true,
  baseUrl: '',
  pageSize: 20,
  chartOptions: { responsive: true },
  chartColors: [
    { // pendientes
      backgroundColor: 'rgba(52, 58, 64, 0.2)',
      borderColor: '#343a40',
      pointBackgroundColor: '#343a40',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(52, 58, 64, 0.2)'
    },
    { // enviados
      backgroundColor: 'rgba(0, 123, 255, 0.2)',
      borderColor: '#007bff',
      pointBackgroundColor: '#007bff',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(0, 123, 255, 0.2)'
    },
    { // entregados
      backgroundColor: 'rgba(40, 167, 69, 0.2)',
      borderColor: '#28a745',
      pointBackgroundColor: '#28a745',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(40, 167, 69, 0.2)'
    },
    { // cancelados
      backgroundColor: 'rgba(220, 53, 69, 0.2)',
      borderColor: '#dc3545',
      pointBackgroundColor: '#dc3545',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(220, 53, 69, 0.2)'
    }
  ]
};
